package business

import geography.City
import geography.Province
import security.User

class Client {
    String name
    String identification
    String address
    String phones
    String email
    Date birthdate
    City city
    String contactPersonName
    String contactPersonPhone
    String contactPersonEmail
    String observations
    String status
    User user

    String type

    Date dateCreated
    Date lastUpdated

    static constraints = {
        identification unique: true
        email nullable: true
        birthdate nullable: true
        contactPersonName nullable: true
        contactPersonPhone nullable: true
        contactPersonEmail nullable: true
        observations nullable: true
        user nullable: true
    }

    static mapping = {
        version false
        observations type: 'text'
        address type: 'text'
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

    void setAddress(String s){
        address = s?.toUpperCase()
    }

    void setEmail(String s){
        email = s?.toUpperCase()
    }

    void setContactPersonName(String s){
        contactPersonName = s?.toUpperCase()
    }

    void setContactPersonEmail(String s){
        contactPersonEmail = s?.toUpperCase()
    }

    void setObservations(String s){
        observations = s?.toUpperCase()
    }

}
