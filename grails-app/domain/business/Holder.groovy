package business

class Holder {

    ContainerClass containerClass
    Plan plan
    String identfication
    String name
    String gender
    Date birthdate
    String certificateNumber
    Date dateCreated
    Date lastUpdated
    String status
    Date exclusionDate
    String observations
    String coverage

    static constraints = {
        exclusionDate nullable: true
        observations nullable: true
    }

    static mapping = {
        version false
        observations type: 'text'
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

    void setObservations(String s){
        observations = s?.toUpperCase()
    }
}
