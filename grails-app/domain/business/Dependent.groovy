package business

class Dependent {

    Holder holder
    String identfication
    String name
    String gender
    Date birthdate
    Date dateCreated
    Date lastUpdated
    String status
    String observations
    Date exclusionDate

    static constraints = {
        exclusionDate nullable: true
        observations nullable: true
    }

    static mapping = {
        version false
        observations type: 'text'
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

    void setObservations(String s){
        observations = s?.toUpperCase()
    }

}
