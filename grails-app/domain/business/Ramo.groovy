package business

class Ramo {

    String name
    Insurance insurance
    String status

    static constraints = {

    }

    static mapping = {
        version false
    }

    void setName(String s){
        name = s?.toUpperCase()
    }
}
