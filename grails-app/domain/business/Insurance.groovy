package business

class Insurance {

    String ruc
    String name
    String phone
    String fax
    String address
    String contactName
    String contactPhone
    String contactEmail
    Date dateCreated
    Date lastUpdated

    static constraints = {
        ruc unique: true
        contactEmail nullable: true, email: true
        contactPhone nullable: true
        fax nullable: true
    }

    static mapping = {
        version false
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

    void setAddress(String s){
        address = s?.toUpperCase()
    }

    void setContactName(String s){
        contactName = s?.toUpperCase()
    }

    void setContactEmail(String s){
        contactEmail = s?.toUpperCase()
    }
}
