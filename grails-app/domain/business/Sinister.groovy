package business

class Sinister {

    Client client
    Diagnosis diagnosis
    String type
    double invoiceValue
    String observations

    static constraints = {
        observations nullable:true
        type inList:['CONSULTA','HONORARIOS', 'MEDICINAS', 'EXAMEN_LABORATORIO', 'EXAMEN_IMAGEN', 'TERAPIA', 'INSUMOS', 'HOSPITAL']
    }

    static mapping = {
        version false
        observations type: 'text'
    }
}
