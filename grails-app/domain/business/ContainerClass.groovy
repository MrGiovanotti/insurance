package business

class ContainerClass {

    String name
    String documentNumber
    double rate
    Policy policy

    static constraints = {
    }

    static mapping = {
        version false
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

}
