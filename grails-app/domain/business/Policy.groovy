package business

import security.User

class Policy {

    Client client
    Ramo ramo
    Product product
    Insurance insurance
    User user
    String number
    Date dateCreated
    Date lastUpdated
    Date startDate
    Date endDate
    Date renovationDate
    String policyType
    String observations
    int comLife
    int comHealth
    int comDental


    static constraints = {
        policyType inList:['IN','CO']
        endDate nullable: true
        startDate nullable: true
        renovationDate nullable: true
    }

    static mapping = {
        version false
        observations type: 'text'
    }
}
