package business

class Product {

    Ramo ramo
    String name
    String description

    static constraints = {
        description nullable:true
    }

    static mapping = {
        version false
        description type: 'text'
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

    void setDescription(String s){
        description = s?.toUpperCase()
    }
}
