package geography

class Province {

    String name

    static constraints = {
        name unique: true
    }

    static mapping = {
        version false
    }

    void setName(String s){
        name = s?.toUpperCase()
    }
}
