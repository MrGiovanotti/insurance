package geography

class City {

    String name
    Province province

    static constraints = {
    }

    static mapping = {
        version false
    }

    void setName(String s){
        name = s?.toUpperCase()
    }

}
