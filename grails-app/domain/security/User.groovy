package security

class User {

    String name
    String username
    String password
    boolean isActive
    Rol rol

    static constraints = {
        username unique: true

    }

    static mapping = {
        password column: '`password`'
        version false
    }
}
