package security

class Permission {

    String actionUrl
    String alias
    boolean allowAssign

    static constraints = {
        actionUrl unique: true
        alias unique: true
    }

    static mapping = {
        version false
    }

}
