package security

class RolPermission {

    Rol rol
    Permission permission

    static constraints = {
    }

    static mapping = {
        version false
    }

}
