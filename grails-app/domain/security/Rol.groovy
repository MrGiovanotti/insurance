package security

class Rol {

    String name
    boolean isActive
    boolean allowModify

    static constraints = {
        name unique: true
    }

    static mapping = {
        version false
    }

}
