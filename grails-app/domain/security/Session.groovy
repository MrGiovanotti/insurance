package security

class Session {

    String sessionId
    Date dateCreated
    Date endDate
    User user

    static constraints = {
        endDate nullable: true
    }

    static mapping = {
        version false
    }

}
