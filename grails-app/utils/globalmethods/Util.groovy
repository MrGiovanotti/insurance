package globalmethods

import business.Client
import business.Insurance
import business.Product
import business.Ramo
import geography.City
import security.Permission
import security.Rol
import security.RolPermission
import security.Session
import security.User

class Util {

    static boolean isAdmin(String username){
        boolean isAdmin = false
        def user = User.findByUsername(username)
        if(user.rol.name.equalsIgnoreCase("ADMINISTRADOR"))
            isAdmin = true
        return isAdmin
    }

    static boolean checkAccess(String username, String action){
        boolean accessAllowed = isAdmin(username)
        User user = User.findByUsername(username)
        def permissionsByRol = RolPermission.findAllByRol(user.rol)
        Permission permission = Permission.findByActionUrl(action)

        for(perm in permissionsByRol){
            if(perm.permission?.actionUrl.trim().equalsIgnoreCase(permission?.actionUrl)){
                accessAllowed = true
                break
            }
        }
        return accessAllowed
    }

    static int getUsuariosLogueados(){

        Date startDate = Date.parse("yyyy-MM-dd HH:mm:ss", new Date().format("yyyy-MM-dd 00:00:00"))
        Date endDate = Date.parse("yyyy-MM-dd HH:mm:ss", new Date().format("yyyy-MM-dd 23:59:59"))
        def users = Session.executeQuery("select distinct s.user, max(s.dateCreated) from Session s where s.dateCreated between :startDate and :endDate and s.user.isActive = true group by s.user", [startDate: startDate, endDate: endDate])
        int contador = 0
        for(int i = 0; i < users.size(); i++){
            Session session = Session.findByUserAndDateCreated(users[i][0], users[i][1])
            if (session.endDate == null)
                contador++
        }

        return contador

    }

    static ArrayList<City> getCitiesByProvince(Client client){
        def cities
        if(client != null){
            cities = City.executeQuery("from City where province = :province", [province: client.city?.province])
        }
        return cities
    }

    static ArrayList<Insurance> getRamosByProduct(Product product){
        def ramos
        if(product != null){
            ramos = Ramo.executeQuery("from Ramo where insurance = :insurance", [insurance: product.ramo?.insurance])
        }
        return  ramos
    }

}
