package geography

class CityController {

    def index() {
        [cities: City.list()]
    }

    def create(){
        respond new City(params)
    }

    def save(City city){
        city.save(flush: true)
        redirect([controller: 'city', action: 'index'])
    }

    def edit(City city){
        respond city
    }

    def update(City city){
        city.save(flush: true)
        redirect([controller: 'city', action: 'index'])
    }
}
