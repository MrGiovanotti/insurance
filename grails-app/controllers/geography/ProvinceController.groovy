package geography

class ProvinceController {

    def index() {
        [provinces: Province.list()]
    }

    def create(){
        respond new Province(params)
    }

    def save(Province province){
        province.save(flush: true)
        redirect([controller: 'province', action: 'index'])
    }

    def edit(Province province){
        respond province
    }

    def update(Province province){
        province.save(flush: true)
        redirect([controller: 'province', action: 'index'])
    }

}
