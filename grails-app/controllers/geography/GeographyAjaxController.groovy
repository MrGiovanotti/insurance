package geography

import grails.converters.JSON

class GeographyAjaxController {

    def getCities() {
        if (params.id) {
            Province province = Province.findById(params.id)
            def cities = City.executeQuery("from City where province = :province order by name", [province: province])
            def array = [cities.id, cities.name]
            render array as JSON
        }
    }
}
