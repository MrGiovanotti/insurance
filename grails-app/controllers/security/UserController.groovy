package security

class UserController {

    def dashboard() {
        Date startDate = Date.parse("yyyy-MM-dd HH:mm:ss", new Date().format("yyyy-MM-dd 00:00:00"))
        Date endDate = Date.parse("yyyy-MM-dd HH:mm:ss", new Date().format("yyyy-MM-dd 23:59:59"))
        def startSession = Session.executeQuery("select user.name, time(dateCreated), time(endDate) from Session where dateCreated between :startDate and :endDate order by dateCreated desc", [startDate: startDate, endDate: endDate])
        [startSession: startSession]
    }

    def login(){
        if(session.user){
            redirect(controller: 'user', action:'dashboard')
        }
        if(params.username && params.password){
            User user = User.findByUsernameAndPasswordAndIsActive(params.username.toString(), params.password.toString(), true)
            if(user){
                session.user = user
                Session SystemSession = new Session()
                SystemSession.sessionId = session.id
                SystemSession.user = user
                SystemSession.save(flush: true)
                redirect(controller: 'user', action:'dashboard')
            }else {
                flash.errorMessage = "Error al iniciar sesión"
                user = new User(params)
                [user: user]
            }
        }
    }

    def logout(){
        Session SystemSession = Session.findBySessionId(session.id)
        if (SystemSession){
            SystemSession.endDate = new Date()
            SystemSession.save(flush: true)
        }

        session.invalidate()
        redirect(action:'login')
    }

    def index(){
        ArrayList<User> userArrayList = new ArrayList<>()
        if(globalmethods.Util.isAdmin(session.user.username.toString())){
            userArrayList = User.findAll()
        }else{
            userArrayList = User.findAll("from User where rol != :rol", [rol: Rol.findByName("ADMINISTRADOR")])
        }
        [users: userArrayList]
    }

    def show(User user){
        [user: user]
    }

    def create(){

    }

    def save(){
        User user = new User(params)
        if(!user.save(flush: true))
            println user.errors
        redirect([controller: 'user', action: 'index'])
    }

    def edit(User user){
        [user: user]
    }

    def update(User user){
        user.save(flush: true)
        redirect([controller: 'user', action: 'index'])
    }

}
