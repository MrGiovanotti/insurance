package security

class PermissionController {

    def index() {
        [permissions: Permission.findAll("from Permission where allowAssign = :allow", [allow: true])]
    }

    def show(Permission permission){
        [permission: permission]
    }

    def create(){

    }

    def save(){
        println params
        Permission permission = new Permission(params)
        permission.allowAssign = true
        permission.save(flush: true)
        redirect([controller: 'permission', action: 'index'])
    }

    def edit(Permission permission){
        [permission: permission]
    }

    def update(Permission permission){
        permission.save(flush: true)
        redirect([controller: 'permission', action: 'index'])
    }

}
