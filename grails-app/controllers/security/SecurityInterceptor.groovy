package security


class SecurityInterceptor {

    SecurityInterceptor(){
        matchAll().excludes(controller: 'access').excludes(controller: 'geographyAjax').excludes(controller: 'businessAjax')
//                .excludes(controller:'access', action:'withOutPermission')
    }

    boolean before() {
        String action = controllerName+'/'+actionName
        if(!action.equalsIgnoreCase("user/login") && !action.equalsIgnoreCase("user/logout") && !action.equalsIgnoreCase("access/withOutPermission")){
            if(!session.user){
                redirect(uri: "/user/login")
                return false
            }else {
                boolean accessAllow = globalmethods.Util.checkAccess(session.user.username.toString(), action)
                if (!accessAllow){
                    println action
                    redirect(uri: "/access/withOutPermission")
                }
            }
        }
        true
    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
