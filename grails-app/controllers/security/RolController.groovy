package security

class RolController {

    def index() {
        ArrayList<Rol> rolList = new ArrayList<>()
        if(globalmethods.Util.isAdmin(session.user.username.toString())){
            rolList = Rol.findAll()
        }else{
            rolList = Rol.findAll("from Rol where name != 'ADMINISTRADOR'")
        }
        [rolList: rolList]
    }
}
