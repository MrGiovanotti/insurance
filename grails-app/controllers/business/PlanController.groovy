package business

class PlanController {

    def index() {
        [plan: Plan.list()]
    }

    def show(Plan plan){
        respond plan
    }

    def create(){
        respond new Plan(params)
    }

    def save(Plan plan){
        if(!plan.save(flush: true)){
            println plan.errors
        }
        redirect([controller: 'plan', action: 'index'])
    }

    def edit(Plan plan){
        respond plan
    }

    def update(Plan plan){
        plan.save(flush: true)
        redirect([controller: 'plan', action: 'index'])
    }
}
