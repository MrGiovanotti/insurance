package business

class RamoController {

    def index() {
        [ramo: Ramo.list()]
    }

    def show(Ramo ramo){
        respond ramo
    }

    def create(){
        respond new Ramo(params)
    }

    def save(Ramo ramo){
        if(!ramo.save(flush: true)){
            println ramo.errors
        }
        redirect([controller: 'ramo', action: 'index'])
    }

    def edit(Ramo ramo){
        respond ramo
    }

    def update(Ramo ramo){

        if(ramo.save(flush: true)){}
        redirect([controller: 'ramo', action: 'index'])
    }
}
