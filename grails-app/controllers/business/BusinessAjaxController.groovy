package business

import geography.City
import geography.Province
import grails.converters.JSON
import security.User

class BusinessAjaxController {

    def getRamosByInsurance(){
        if(params.id){
            Insurance insurance = Insurance.findById(params.id.toString().toLong())
            def ramos = Ramo.executeQuery("from Ramo where insurance = :insurance and status = 'ACTIVE' order by name", [insurance: insurance])
            def array = [ramos.id, ramos.name]
            render array as JSON
        }
    }

    def getProductsByRamo(){
        if(params.id){
            Ramo ramo = Ramo.findById(params.id.toString().toLong())
            def products = Product.executeQuery("from Product where ramo = :ramo order by name", [ramo: ramo])
            def array = [products.id, products.name]
            render array as JSON
        }
    }

    //Ajax
    def searchCustomer(){
        if(request.xhr){
            def customer = Client.findByIdentification(params.identification)
            render customer as JSON
        }
    }

    //Ajax
    def addNewCustomer(){
        if(request.xhr){
            def customer = Client.findByIdentification(params.identification)

            if(customer){
                render "Cliente ya se encuentra agregado en el sistema."
            }else{
                def client = new Client()
                client.identification = params.identification
                client.name = params.name
                client.province = Province.findById(params.province)
                client.city = City.findById(params.city)
                client.type = params.type
                client.birthdate = new Date().parse("yyyy-MM-dd HH:mm:ss", params.birthday.toString().replace('/', '-') + " 00:00:00")
                client.phones = params.phone
                client.email = params.mail
                client.address = params.address
                client.contactPersonName = params.contactPName
                client.contactPersonPhone = params.contactPPhone
                client.contactPersonEmail = params.contactPMail
                client.status = params.status
                client.observations = params.observations
                client.user = User.findById(session.user.id.toString().toLong())

                if(client.save(flush:true)){
                    render "true"
                }else{
                    render "Ha ocurrido un error, por favor intentelo de nuevo."
                }
            }
        }
    }

    def getClassContainer(){
        if(request.xhr){
            def policyId = Policy.findByNumber(params.idNumber)

            def classId = ContainerClass.findAllByPolicy(policyId)

            //get all relationship
            JSON.use('deep') {
                render classId as JSON
            }
        }
    }

    def searchPolicy(){
        if(request.xhr){
            def policy = Policy.findByNumber(params.policy)

            def result = [
                    id : policy.id,
                    number : policy.number,
                    identification : policy.client.identification,
                    name : policy.client.name,
                    startPolicy : policy.startDate,
                    endPolicy : policy.endDate,
                    ramo : policy.ramo.name,
                    product : policy.product.name
            ]

            render result as JSON
        }
    }

    def searchHolders(){
        if(request.xhr){
            def policyNumber = Policy.findByNumber(params.policyNumber)
            def classId = ContainerClass.findAllByPolicy(policyNumber)
            def holders = Holder.findAllByContainerClass(classId)
            render holders as JSON
        }
    }

    def getInformationHolder(){
        if(request.xhr){
            def holder = Holder.findById(params.holderId)

            render holder as JSON

        }
    }

}
