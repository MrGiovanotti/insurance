package business

class DiagnosisController {

    def index() {
        [diagnosis: Diagnosis.list()]
    }

    def show(Diagnosis diagnosis){
        respond diagnosis
    }

    def create(){
        respond new Diagnosis(params)
    }

    def save(Diagnosis diagnosis){
        if(!diagnosis.save(flush: true)){
            println diagnosis.errors
        }
        redirect([controller: 'diagnosis', action: 'index'])
    }

    def edit(Diagnosis diagnosis){
        respond diagnosis
    }

    def update(Diagnosis diagnosis){
        diagnosis.save(flush: true)
        redirect([controller: 'diagnosis', action: 'index'])
    }
}
