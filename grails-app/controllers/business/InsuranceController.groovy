package business

class InsuranceController {

    def index() {
        [insurances: Insurance.list()]
    }

    def show(Insurance insurance){
        respond insurance
    }

    def create(){
        respond new Insurance(params)
    }

    def save(Insurance insurance){

        if(!insurance.save(flush: true)){
            println insurance.errors
        }
        redirect([controller: 'insurance', action: 'index'])
    }

    def edit(Insurance insurance){
        respond insurance
    }

    def update(Insurance insurance){
        insurance.save(flush: true)
        redirect([controller: 'insurance', action: 'index'])
    }
}
