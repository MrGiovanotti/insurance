package business

import grails.converters.JSON

class SinisterController {

    def index() {
        [plan: Sinister.list()]
    }

    def show(Sinister sinister){
        respond sinister
    }

    def create(){
        respond new Sinister(params)
    }

    def save(Sinister sinister){
        if(!sinister.save(flush: true)){
            println sinister.errors
        }

        redirect([controller: 'sinister', action: 'index'])
    }

    def edit(Sinister sinister){
        respond sinister
    }

    def update(Sinister sinister){
        sinister.save(flush: true)
        redirect([controller: 'sinister', action: 'index'])
    }
}
