package business

import security.User


class ClientController {

    def index() {
        [clients: Client.list()]
    }

    def show(Client client){
        respond client
    }

    def create(){
        respond new Client(params)
    }

    def save(Client client){

        client.user = User.findById(session.user.id.toString().toLong())

        if(params.birthdatetemp && params.birthdatetemp != "")
            client.birthdate = new Date().parse("yyyy-MM-dd HH:mm:ss", params.birthdatetemp.toString().replace('/', '-') + " 00:00:00")

        if(!client.save(flush: true)){
            println client.errors
        }
        redirect([controller: 'client', action: 'index'])
    }

    def edit(Client client){
        respond client
    }

    def update(Client client){
        client.birthdate = new Date().parse("yyyy-MM-dd HH:mm:ss", params.birthdatetemp.toString().replace('/', '-') + " 00:00:00")
        client.save(flush: true)
        redirect([controller: 'client', action: 'index'])
    }
}
