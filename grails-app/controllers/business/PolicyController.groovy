package business

import geography.City
import geography.Province
import grails.converters.JSON
import security.User
import groovy.time.TimeCategory

class PolicyController {

    def index() {
        [policy: Policy.list()]
    }

    def create(){
        respond new Policy(params)
    }

    def save(Policy policy){

        //Save Policy
        policy.number = params.number
        policy.startDate = new Date().parse("yyyy-MM-dd HH:mm:ss", params.startDateTemp.toString().replace('/', '-') + " 00:00:00")
        policy.endDate = new Date().parse("yyyy-MM-dd HH:mm:ss", params.endDateTemp.toString().replace('/', '-') + " 00:00:00")
        policy.user = User.findById(session.user.id.toString().toLong())
        policy.insurance = Insurance.findById(params.insurance)
        policy.ramo = Ramo.findById(params.ramo)
        policy.product = Product.findById(params.product)
        policy.comLife = params.life.toString().toInteger()
        policy.comHealth= params.health.toString().toInteger()
        policy.comDental = params.dental.toString().toInteger()
        policy.observations = params.observations ? params.observations : ""
        policy.policyType = params.membershipType
        policy.client = Client.findById(1) //TODO
        policy.renovationDate = policy.startDate


        if(!policy.save(flush: true)){
            println policy.errors
        }else{
            if(params.list('afi[classId]').size() >= 1 && params.list('afi[identification]').size() >= 1 && params.list('afi[plan]').size() >= 1){
                for(affiliate in [(params.list('afi[classId]')),
                                  (params.list('afi[identification]'))].transpose()){

                    //Save Class
                    def classContainer = new ContainerClass()
                        classContainer.policy =  Policy.findById(policy.id)
                        classContainer.name = affiliate.get(0).toString()
                        classContainer.documentNumber = affiliate.get(1).toString()
                        classContainer.save()
                }
            }

            if(params.list('holder[tidentification]').size() >= 1 && params.list('holder[tname]').size() >= 1 && params.list('holder[tgender]').size() >= 1){
                for(holderRec in [(params.list('holder[tidentification]')),
                                  (params.list('holder[tname]')),
                                  (params.list('holder[tgender]')),
                                  (params.list('holder[tbirthday]')),
                                  (params.list('holder[tcertified]')),
                                  (params.list('holder[tclass]')),
                                  (params.list('holder[tplan]')),
                                  (params.list('holder[tcoverage]'))].transpose()){

                    def policyId = Policy.findByNumber(params.number)

                    //Save Holder
                    def holder = new Holder()
                        holder.identfication =  holderRec.get(0).toString()
                        holder.name = holderRec.get(1).toString()
                        holder.gender = holderRec.get(2).toString()
                        holder.status = "ACTIVE"
                        holder.observations = ""
                        holder.birthdate = new Date().parse("yyyy-MM-dd HH:mm:ss", holderRec.get(3).toString().replace('/', '-') + " 00:00:00")
                        holder.certificateNumber = holderRec.get(4).toString()
                        holder.exclusionDate = null
                        holder.containerClass = ContainerClass.findByNameAndPolicy(holderRec.get(5).toString(), policyId)
                        holder.plan = Plan.findById(holderRec.get(2).toString())
                        holder.coverage = holderRec.get(1).toString()
                        holder.save()
                }
            }

            if(params.list('depend[didentification]').size() >= 1 && params.list('depend[dname]').size() >= 1 && params.list('depend[dcertified]').size() >= 1){
                for(dependRec in [(params.list('depend[didentification]')),
                                  (params.list('depend[dname]')),
                                  (params.list('depend[dgender]')),
                                  (params.list('depend[dbirthday]')),
                                  (params.list('depend[dcertified]')),
                                  (params.list('depend[dcoverage]')),
                                  (params.list('depend[dholder]'))].transpose()){

                    //Save Depend
                    def depend = new Dependent()
                        depend.identfication = dependRec.get(0).toString()
                        depend.name = dependRec.get(1).toString()
                        depend.gender = dependRec.get(2).toString()
                        depend.status = 'ACTIVE'
                        depend.observations = ''
                        depend.birthdate = new Date().parse("yyyy-MM-dd HH:mm:ss", dependRec.get(3).toString().replace('/', '-') + " 00:00:00")
                        depend.holder = Holder.findByIdentfication(dependRec.get(6).toString())
                        depend.exclusionDate = null
                        depend.save()
                }
            }
        }
        redirect(uri: "/policy/index")
    }

    def edit(Policy policy){
        respond policy
    }

    def update(Policy policy){

    }

    def show(Policy policy){
        respond policy
    }

}