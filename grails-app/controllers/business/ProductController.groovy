package business

class ProductController {

    def index() {
        [product: Product.list()]
    }

    def show(Product product){
        respond product
    }

    def create(){
        respond new Product(params)
    }

    def save(Product product){
        if(!product.save(flush: true)){
            println product.errors
        }
        redirect([controller: 'product', action: 'index'])
    }

    def edit(Product product){
        respond product
    }

    def update(Product product){
        product.save(flush: true)
        redirect([controller: 'product', action: 'index'])
    }
}
