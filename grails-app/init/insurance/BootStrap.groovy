package insurance

import security.Rol
import security.User

class BootStrap {

    def init = { servletContext ->

        new Rol(name: "ADMINISTRADOR").save()

        def rol = Rol.findByName("ADMINISTRADOR")
        def user = new User(isActive: true, rol: rol, username: 'admin', name: 'Administrador', password: 'admin')
        user.save()

    }
    def destroy = {
    }
}
