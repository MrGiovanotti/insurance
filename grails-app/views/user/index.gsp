<meta name="layout" content="main">
<div class="linkNew"><g:link class="btn btn-default" controller="user" action="create">Nuevo Usuario</g:link> </div>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Rol</th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${users}" var="userInstance">
                    <tr>
                        <td><g:link controller="user" action="edit" id="${userInstance.id}">${userInstance.name}</g:link></td>
                        <td>${userInstance.username}</td>
                        <td>${userInstance.rol.name}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </div>
</div>