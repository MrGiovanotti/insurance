<meta name="layout" content="main"/>
<asset:javascript src="user/dashboard.js"></asset:javascript>

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Dashboard <small></small>
            </h1>
            <ol class="breadcrumb">
                <li class="active">
                    <i class="fa fa-dashboard"></i> Dashboard
                </li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-6 col-xs-12 cuadro">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-user fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">${globalmethods.Util.getUsuariosLogueados()}</div>
                            <div>¡Usuarios!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12 cuadro">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-check fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">5</div>
                            <div>¡Pólizas!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12 cuadro">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-times fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">7</div>
                            <div>¡Siniestros!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-md-6 col-xs-12 cuadro">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-dollar fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge">0</div>
                            <div>¡Exclusiones!</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6 col-xs-12">
            <div class="panel panel-border-blue-u">
                <div class="panel-heading panel-body-blue-u">
                    <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Sesiones</h3>
                    <a class="btn btn-blue-u btn-clickable pull-right" onclick="return false;" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                <tr>
                                    <th>Agente</th>
                                    <th>Inicio</th>
                                    <th>Fin</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${startSession}">
                                    <tr>
                                        <td>${it[0]}</td><td>${it[1]}</td><td>${it[2]?:'Activa'}</td>
                                    </tr>
                                </g:each>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-8 col-md-6 col-xs-12">
            <div class="panel panel-border-yellow">
                <div class="panel-heading panel-body-yellow">
                    <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Gráfico de Ejemplo 1</h3>
                    <a class="btn btn-yellow btn-clickable pull-right" onclick="return false;" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="container" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-12 col-md-6 col-xs-12">
            <div class="panel panel-border-red1">
                <div class="panel-heading panel-body-red1">
                    <h3 class="panel-title"><i class="fa fa-remove fa-fw"></i> Estatus</h3>
                    <a class="btn btn-red1 btn-clickable pull-right" onclick="return false;" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsivex">
                        <div class="col-lg-4" id="containerpieCu1" style="min-width: 310px; height: 350px; max-width: 600px; margin: 0 auto"></div>
                        <div class="col-lg-4" id="containerpieCu2" style="min-width: 310px; height: 350px; max-width: 600px; margin: 0 auto"></div>
                        <div class="col-lg-4" id="containerpieCu3" style="min-width: 310px; height: 350px; max-width: 600px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-6 col-xs-12 col-xs-12">
            <div class="panel panel-border-orange">
                <div class="panel-heading panel-body-orange">
                    <h3 class="panel-title"><i class="fa fa-check fa-fw"></i> Pie Drilldown</h3>
                    <a class="btn btn-orange btn-clickable pull-right" onclick="return false;" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="containerpiedd" style="min-width: 310px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="panel panel-border-lila">
                <div class="panel-heading panel-body-lila">
                    <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Grafico de lineas</h3>
                    <a class="btn btn-lila btn-clickable pull-right" onclick="return false;" href="#">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <div id="containerlinereport" style="min-width: 310px; height: 400px; max-width: 1000px; margin: 0 auto"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>




</div>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
