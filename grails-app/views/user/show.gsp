<meta name="layout" content="main">
<div class="panel-group">
    <div class="panel panel-info">
        <div class="panel-heading">
            <strong>Datos del Usuario</strong>
        </div>
        <div class="panel-body">
            <h1>Mostrando datos del usuario</h1>
        </div>
    </div>
    <div class="panel-footer">
        <div>
            <g:link class="btn btn-primary" controller="user" action="edit" id="${user?.id}">Editar</g:link>
        </div>
    </div>
</div>