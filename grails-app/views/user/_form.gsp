<div class="col-lg-12">
    <div class="col-lg-3 form-group">
        <label>Nombre: </label>
        <g:textField class="form-control" name="name" value="${user?.name}"></g:textField>
    </div>
    <div class="col-lg-3 form-group">
        <label>Usuario: </label>
        <g:textField class="form-control" name="username" value="${user?.username}"></g:textField>
    </div>
    <div class="col-lg-3 form-group">
        <label>Password: </label>
        <g:textField class="form-control" name="password" value="${user?.password}"></g:textField>
    </div>
    <div class="col-lg-3 form-group">
        <label>Rol: </label>
        <g:if test="${globalmethods.Util.isAdmin(session.user.username.toString())}">
            <g:select class="form-control" name="rol" optionKey="id" value="${user?.rol?.id}" from="${security.Rol.findAll("from Rol where isActive = :isActive", [isActive: true])}" optionValue="${{it.name}}" noSelection="${['': '-- Seleccione --']}"></g:select>
        </g:if>
        <g:else>
            <g:select class="form-control" name="rol" optionKey="id" value="${user?.rol?.id}" from="${security.Rol.findAll("from Rol where name != 'ADMINISTRADOR' and isActive = :isActive", [isActive: true])}" optionValue="${{it.name}}" noSelection="${['': '-- Seleccione --']}"></g:select>
        </g:else>
    </div>
    <div class="col-lg-3 form-group">
        <label>Estado: </label>
        <g:select class="form-control" name="isActive" value="${user?.isActive}" optionKey="key" optionValue="value" from="${[true: 'ACTIVO', false: 'INACTIVO']}"></g:select>
    </div>
</div>