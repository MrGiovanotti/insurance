<meta name="layout" content="main">
<div><g:link controller="rol" action="create">Nuevo Rol</g:link> </div>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Activo</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${rolList}" var="rolInstance">
                <tr>
                    <td><g:link controller="user" action="show" id="${rolInstance.id}">${rolInstance.name}</g:link></td>
                    <td><g:link controller="user" action="show" id="${rolInstance.id}">${rolInstance.isActive}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>