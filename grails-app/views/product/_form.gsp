<%@ page import="business.Insurance" %>
<asset:javascript src="usogeneral/insuranceRamoProductLogic.js"></asset:javascript>
<div class="container-fluid">
    <div class="form-group col-lg-3">
        <label>Nombre</label>
        <g:textField class="form-control" name="name" value="${product?.name}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Aseguradora:</label>
        <g:select class="form-control" required="" name="insurance" optionKey="id" from="${business.Insurance.list()}" noSelection="${['': '-- Seleccione --']}" optionValue="name" value="${product?.ramo?.insurance?.id}"></g:select>
    </div>
    <div class="form-group col-lg-3">
        <label>Ramo:</label>
        <g:select class="form-control" required="" name="ramo" optionKey="id" from="${globalmethods.Util.getRamosByProduct(product)}" noSelection="${['': '-- Seleccione --']}" optionkey="id" optionValue="name" value="${product?.ramo?.id}"></g:select>
    </div>
    <div class="form-group col-lg-12">
        <label>Descripcion:</label>
        <g:textArea class="form-control" name="description" />
    </div>
</div>