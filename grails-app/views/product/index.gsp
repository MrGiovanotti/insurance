<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="product" action="create">Nuevo Producto</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Ramo</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${product}" var="productInstance">
                <tr>
                    <td><g:link controller="product" action="edit" id="${productInstance.id}">${productInstance.name}</g:link></td>
                    <td>${productInstance.ramo.name}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>