<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="province" action="create">Nueva Provincia</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${provinces}" var="provinceInstance">
                <tr>
                    <td><g:link controller="province" action="edit" id="${provinceInstance.id}">${provinceInstance.name}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>