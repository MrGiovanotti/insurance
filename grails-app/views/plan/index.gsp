<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="plan" action="create">Nuevo Plan</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${plan}" var="planInstance">
                <tr>
                    <td><g:link controller="plan" action="edit" id="${planInstance.id}">${planInstance.name}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>