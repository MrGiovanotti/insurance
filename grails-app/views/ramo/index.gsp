<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="ramo" action="create">Nuevo Ramo</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Aseguradora</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${ramo}" var="ramoInstance">
                <tr>
                    <td><g:link controller="ramo" action="edit" id="${ramoInstance.id}">${ramoInstance.name}</g:link></td>
                    <td>${ramoInstance.insurance.name}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>