<%@ page import="business.Insurance" %>
<div class="container-fluid">
    <div class="form-group col-lg-3">
        <label>Nombre</label>
        <g:textField class="form-control" name="name" value="${ramo?.name}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Aseguradora:</label>
        <g:select class="form-control" required="" name="insurance" optionKey="id" from="${business.Insurance.list()}" noSelection="${['': '-- Seleccione --']}" optionValue="name" value="${ramo?.insurance?.id}"></g:select>
    </div>
    <div class="form-group col-lg-3">
        <label>Estado:</label>
        <g:select class="form-control" name="status" from="${['ACTIVE':'ACTIVO', 'INACTIVE':'INACTIVO']}" optionKey="key" optionValue="value" value="${ramo?.status}"/>
    </div>
</div>