<meta name="layout" content="main">

<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <g:form url="[resource:ramo, action:'update']" method="PUT">
                <g:render template="form"></g:render>
                <g:link class="btn btn-primary" url="/ramo/index" value="Atrás">Atrás</g:link>
                <g:submitButton class="btn btn-primary" name="btnSubmit" value="Actualizar"></g:submitButton>
            </g:form>
        </div>
    </div>
</div>