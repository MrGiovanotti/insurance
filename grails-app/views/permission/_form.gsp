<div class="col-lg-12">
    <div class="col-lg-4">
        <label>Action: </label>
        <g:textField name="actionUrl" value="${permission?.actionUrl}"></g:textField>
    </div>
    <div class="col-lg-4">
        <label>Alias: </label>
        <g:textField name="alias" value="${permission?.alias}"></g:textField>
    </div>
</div>