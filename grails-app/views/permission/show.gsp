<meta name="layout" content="main">
<div>
    <div>
        Action: ${permission.actionUrl}
    </div>
    <div>
        Alias: ${permission.alias}
    </div>
    <div>
        <g:link controller="permission" action="edit" id="${permission?.id}">Editar</g:link>
    </div>
</div>