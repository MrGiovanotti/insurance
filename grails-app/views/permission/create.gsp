<meta name="layout" content="main">

<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <g:form action="save">
                <g:render template="form"></g:render>
                <g:submitButton class="btn-save" name="btnSubmit" value="Guardar"></g:submitButton>
            </g:form>
        </div>
    </div>
</div>