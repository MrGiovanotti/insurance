<meta name="layout" content="main">
<div><g:link controller="permission" action="create">Nuevo Permiso</g:link> </div>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Action</th>
                <th>Alias</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${permissions}" var="permissionInstance">
                <tr>
                    <td><g:link controller="permission" action="show" id="${permissionInstance.id}">${permissionInstance.actionUrl}</g:link></td>
                    <td><g:link controller="permission" action="show" id="${permissionInstance.id}">${permissionInstance.alias}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>