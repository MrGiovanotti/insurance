<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="client" action="create">Nuevo Cliente</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Identificación</th>
                <th>Tipo</th>
                <th>Fecha Creación</th>
                <th>Creado por</th>
                <th>Estado</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${clients}" var="clientInstance">
                <tr>
                    <td><g:link controller="client" action="show" id="${clientInstance.id}">${clientInstance.name}</g:link></td>
                    <td>${clientInstance.identification}</td>
                    <td>${clientInstance.type}</td>
                    <td>${clientInstance.dateCreated}</td>
                    <td>${clientInstance.user.name}</td>
                    <td>${clientInstance.status}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>