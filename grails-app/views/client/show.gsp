<meta name="layout" content="main">
<div class="container-fluid">
    <div class="panel-group">
        <div class="panel panel-primary">
            <div class="panel-body">
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Estado: </strong>${client.status}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Identificación: </strong>${client.identification}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Nombre: </strong>${client.name}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Teléfonos: </strong>${client.phones}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Dirección: </strong>${client.address}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Email: </strong>${client.email}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Fecha de Nacimiento: </strong>${client.birthdate?.toString()?.substring(0, 10)}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Provincia: </strong>${client.city.province.name}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Ciudad: </strong>${client.city.name}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Persona de Contacto: </strong>${client.contactPersonName}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Teléfono de contacto: </strong>${client.contactPersonPhone}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Email de Contacto: </strong>${client.contactPersonEmail}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Fecha Creación: </strong>${client.dateCreated}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Creado por: </strong>${client.user.name}
                </div>
                <div class="form-group col-lg-4 col-md-6 col-xs-12">
                    <strong>Fecha de Actualización: </strong>${client.lastUpdated}
                </div>
                <div class="form-group col-lg-12 col-md-6 col-xs-12">
                    <strong>Observaciones: </strong>${client.observations}
                </div>
                <div class="form-group col-lg-12 col-md-6 col-xs-12">
                    <g:link class="btn btn-primary" controller="client" action="edit" id="${client.id}">Editar</g:link>
                </div>
            </div>
        </div>

    </div>
</div>