<asset:javascript src="usogeneral/bootstrap-datepicker.min.js" />
<asset:javascript src="usogeneral/bootstrap-datepicker.es.min.js" />
<asset:stylesheet src="usogeneral/bootstrap-datepicker.min.css" />
<asset:javascript src="usogeneral/customdatepicker.js" />
<asset:javascript src="usogeneral/province.js"></asset:javascript>
<div class="container-fluid">

    <div class="form-group col-lg-3">
        <label>Identificación</label>
        <span class="required-indicator">*</span>
        <g:textField required="" class="form-control" name="identification" value="${client?.identification}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Nombre</label>
        <span class="required-indicator">*</span>
        <g:textField required="" class="form-control" name="name" value="${client?.name}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Provincia</label>
        <span class="required-indicator">*</span>
        <g:select required="" class="form-control" name="province" from="${geography.Province.list()}" noSelection="${['': '-- Seleccione --']}" optionKey="id" optionValue="name" value="${client?.city?.province?.id}"></g:select>
    </div>

    <div class="form-group col-lg-3">
        <label>Ciudad</label>
        <span class="required-indicator">*</span>
        <g:select required="" class="form-control" name="city" from="${globalmethods.Util.getCitiesByProvince(client)}" noSelection="${['': '-- Seleccione --']}" value="${client?.city?.id}" optionKey="id" optionValue="name"></g:select>
    </div>

    <div class="form-group col-lg-3">
        <label>Teléfono</label>
        <span class="required-indicator">*</span>
        <g:textField required="" class="form-control" name="phones" value="${client?.phones}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Email</label>
        <input type="email" class="form-control" name="email" value="${client?.email}"></input>
    </div>

    <div class="form-group col-lg-3">
        <label>Fecha Nacimiento</label>
        <g:textField class="form-control datepicker" name="birthdatetemp" value="${client?.birthdate?.toString()?.replace('-', '/')?.substring(0, 10)}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Persona de Contacto</label>
        <g:textField class="form-control" name="contactPersonName" value="${client?.contactPersonName}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Teléfono de Contacto</label>
        <g:textField class="form-control" name="contactPersonPhone" value="${client?.contactPersonPhone}"></g:textField>
    </div>

    <div class="form-group col-lg-3">
        <label>Email de Contacto</label>
        <input type="email" class="form-control" name="contactPersonEmail" value="${client?.contactPersonEmail}"></input>
    </div>

        <div class="form-group col-lg-3">
            <label>Tipo Persona</label>
            <g:select class="form-control" required="" name="type" from="${['NATURAL', 'JURIDICA']}" noSelection="${['': '-- Seleccione --']}" value="${client?.type}"></g:select>
        </div>

    <div class="form-group col-lg-3">
        <label>Estado</label>
        <g:select class="form-control" required="" name="status" from="${['ACTIVO', 'INACTIVO']}" value="${client?.status}"></g:select>
    </div>

    <div class="form-group col-lg-12">
        <label>Dirección</label>
        <span class="required-indicator">*</span>
        <g:textArea required="" class="form-control" name="address" value="${client?.address}"></g:textArea>
    </div>

    <div class="form-group col-lg-12">
        <label>Observaciones</label>
        <g:textArea class="form-control" name="observations" value="${client?.observations}"></g:textArea>
    </div>

    <input type="hidden" name="user" value="${client != null? client?.user?.id: session?.user?.id}">

</div>