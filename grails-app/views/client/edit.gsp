<meta name="layout" content="main">
<div>
    <g:form url="[resource:client, action:'update']" method="PUT">
        <g:render template="form"></g:render>
        <g:link class="btn btn-primary" url="/client/index" value="Atrás">Atrás</g:link>
        <g:submitButton class="btn btn-primary" name="btnSubmit" value="Actualizar"></g:submitButton>
    </g:form>
</div>