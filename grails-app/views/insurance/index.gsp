<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="insurance" action="create">Nueva Aseguradora</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Ruc</th>
                <th>Nombre</th>
                <th>Teléfono</th>
                <th>Nombre Contacto</th>
                <th>Número Contacto</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${insurances}" var="insuranceInstance">
                <tr>
                    <td><g:link controller="insurance" action="edit" id="${insuranceInstance.id}">${insuranceInstance.ruc}</g:link></td>
                    <td>${insuranceInstance.name}</td>
                    <td>${insuranceInstance.phone}</td>
                    <td>${insuranceInstance.contactName}</td>
                    <td>${insuranceInstance.contactPhone}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>