<div class="container-fluid">
    <div class="form-group col-lg-3">
        <label>Ruc: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="ruc" value="${insurance?.ruc}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Nombre: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="name" value="${insurance?.name}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Telefono: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="phone" value="${insurance?.phone}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Fax: </label>
        <g:textField class="form-control" name="fax" value="${insurance?.fax}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Nombre contacto: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="contactName" value="${insurance?.contactName}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Número contacto: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="contactPhone" value="${insurance?.contactPhone}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Email contacto: </label>
        <span class="required-indicator">*</span>
        <input type="email" class="form-control" required="" name="contactEmail" value="${insurance?.contactEmail}"></input>
    </div>
    <div class="form-group col-lg-12">
        <label>Direccion: </label>
        <span class="required-indicator">*</span>
        <g:textArea class="form-control" required="" name="address" value="${insurance?.address}"></g:textArea>
    </div>
</div>