<%@ page import="business.Insurance" %>

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Informacion Poliza</div>

            <div class="container-fluid">

                <div class="form-group col-lg-3">
                    <label>Buscar Poliza:</label>
                    <g:textField id="spoliza" class="form-control" required="" name="spoliza" value=""></g:textField>
                </div>
                <div class="form-group col-lg-2">
                    <label>&nbsp;</label>
                    <button id="btnSearchSinister" type="button" class="btn btn-primary form-control">Buscar</button>
                </div>
            </div>

            <div class="line"></div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Poliza No</th>
                        <th>Asegurado</th>
                        <th>Ramo</th>
                        <th>Producto</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="dataPolicy">
                        <tr>

                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Datos Cliente</div>

            <div class="container-fluid">

                <div class="form-group col-lg-3">
                    <label>Buscar Poliza:</label>
                    <g:select class="form-control" id="holders" name="holders" from="" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>
                <div class="form-group col-lg-1">
                    <label>&nbsp;</label>
                    <button id="btnAddHolder" type="button" class="btn btn-success form-control">+</button>
                </div>
            </div>
            <div class="line"></div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Identificacion</th>
                        <th>Nombre</th>
                        <th>fecha Nacimiento</th>
                        <th>No Certificado</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="dataCustomer">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Siniestro</div>

            <div class="form-group col-lg-3">
                <label>Nombre Paciente</label>
                <span class="required-indicator">*</span>
                <g:select class="form-control" name="spatient" from="${business.Client.list()}" noSelection="${['': '-- Seleccione --']}" value="${client?.name}" optionKey="id" optionValue="name"></g:select>
            </div>

            <div class="form-group col-lg-3">
                <label>Diagnostico</label>
                <span class="required-indicator">*</span>
                <g:select class="form-control" name="diagnosis" from="${business.Diagnosis.list()}" noSelection="${['': '-- Seleccione --']}" value="${diagnosis?.name}" optionKey="id" optionValue="name"></g:select>
            </div>
            <div class="form-group col-lg-3">
                <label>Tipo</label>
                <span class="required-indicator">*</span>
                <g:select class="form-control" name="stype" from="${['CONSULTA': 'CONSULTA','HONORARIOS':'HONORARIOS', 'MEDICINAS':'MEDICINAS', 'EXAMEN_LABORATORIO':'EXAMEN_LABORATORIO', 'EXAMEN_IMAGEN':'EXAMEN_IMAGEN', 'TERAPIA':'TERAPIA', 'INSUMOS':'INSUMOS', 'HOSPITAL':'HOSPITAL']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
            </div>
            <div class="form-group col-lg-3">
                <label>Valor Factura</label>
                <span class="required-indicator">*</span>
                <g:textField id="invoiceValue" class="form-control" required="" name="invoiceValue" value=""></g:textField>
            </div>
            <div class="form-group col-lg-12">
                <label>Observaciones</label>
                <span class="required-indicator">*</span>
                <g:textArea id="observations" class="form-control" required="" name="observations" value=""></g:textArea>
            </div>
            <div class="form-group col-lg-2">
                <label>&nbsp;</label>
                <button id="btnSinister" type="button" class="btn btn-success form-control">Agregar</button>
            </div>

            <div class="right-form form-group col-lg-2">
                <label>&nbsp;</label>
                <g:textField id="totalInvoice" class="form-control" required="" placeholder="Total Reclamos" name="totalInvoice" value="" readonly="true"></g:textField>
            </div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Codigo Cliente</th>
                        <th>Nombre</th>
                        <th>Diagnostico</th>
                        <th>Tipo</th>
                        <th>Valor Factura</th>
                        <th>Observaciones</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="sinisterTable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

