<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="sinister" action="create">Nuevo Siniestro</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Ramo</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${sinister}" var="sinisterInstance">
                <tr>
                    <td><g:link controller="sinister" action="edit" id="${sinisterInstance.id}">${sinisterInstance.name}</g:link></td>
                    <td>${sinisterInstance.ramo.name}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>