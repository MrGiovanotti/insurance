<div class="container-fluid">
    <div class="form-group col-lg-3">
        <label>Nombre: </label>
        <span class="required-indicator">*</span>
        <g:textField class="form-control" required="" name="name" value="${diagnosis?.name}"></g:textField>
    </div>
    <div class="form-group col-lg-12">
        <label>Descripcion</label>
        <g:textArea class="form-control" name="description" value="${diagnosis?.description}"></g:textArea>
    </div>
</div>