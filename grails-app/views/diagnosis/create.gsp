<meta name="layout" content="main">

<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <g:form action="save">
                <g:render template="form"></g:render>
                <g:link class="btn btn-primary" url="/diagnosis/index" value="Atrás">Atrás</g:link>
                <g:submitButton class="btn btn-primary" name="btnSubmit" value="Guardar"></g:submitButton>
            </g:form>
        </div>
    </div>
</div>