<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="diagnosis" action="create">Nuevo Diagnostico</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${diagnosis}" var="diagnosisInstance">
                <tr>
                    <td><g:link controller="diagnosis" action="edit" id="${diagnosisInstance.id}">${diagnosisInstance.name}</g:link></td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>