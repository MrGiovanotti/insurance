<asset:javascript src="usogeneral/insuranceRamoProductLogic.js"></asset:javascript>

<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="line">Informacion Póliza</div>
                <div class="form-group col-lg-3">
                    <label>Numero Poliza: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="number" value="${policy?.number}"></g:textField>
                </div>

                <div class="form-group col-lg-3">
                    <label>Fecha Inicio</label>
                    <span class="required-indicator">*</span>
                    <g:textField required="" class="form-control datepicker" name="startDateTemp" value="${policy?.startDate?.toString()?.replace('-', '/')?.substring(0, 16)}"></g:textField>
                </div>

                <div class="form-group col-lg-3">
                    <label>Fecha Fin</label>
                    <span class="required-indicator">*</span>
                    <g:textField required="" class="form-control datepicker" name="endDateTemp" value="${policy?.startDate?.toString()?.replace('-', '/')?.substring(0, 16)}"></g:textField>
                </div>

                <div class="form-group col-lg-3">
                    <label>Tipo Afiliación</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="membershipType" from="${['IN': 'Individual','CO':'Colectivo']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>

                <div class="form-group col-lg-3">
                    <label>Aseguradora</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="insurance" from="${business.Insurance.list()}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>

                <div class="form-group col-lg-3">
                    <label>Ramo</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="ramo" from="" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>

                <div class="form-group col-lg-3">
                    <label>Producto</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="product" from="" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-red1">
        <div class="panel-body">
            <div class="line">Comisiones</div>

            <div class="container-fluid">
                <div class="form-group col-lg-3">
                    <label>% Comision Vida: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="life" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>% Comision Salud: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="health" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>% Comision Dental: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="dental" value=""></g:textField>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Clientes/Clases</div>

            <div class="container-fluid">

                <div class="form-group col-lg-3">
                    <label>Buscar/Agregar Cliente:</label>
                    <g:select class="form-control" id="searchby" name="searchby" from="${['CI': 'Cedula-Ruc','BYNAME':'Por Nombre', 'NEW': 'Agregar Nuevo Cliente']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>

                <div id="seachByIdentification">
                    <div class="form-group col-lg-3">
                        <label>Cedula/Ruc Asegurado: </label>
                        <g:textField id="cIdentification" class="form-control" name="cIdentification" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-2">
                        <label>&nbsp;</label>
                        <button id="btnByIdentification" type="button" class="btn btn-primary form-control">Buscar</button>
                    </div>
                </div>

                <div id="seachByName">
                    <div class="form-group col-lg-3">
                        <label>Cedula/Ruc Asegurado: </label>
                        <span class="required-indicator">*</span>
                        <g:select id="cSearchCustomer" name="searchCustomer" from="${business.Client.list()}" optionKey="identification" optionValue="name" class="form-control select2 selectpicker" data-live-search="true"></g:select>
                    </div>
                    <div class="form-group col-lg-1">
                        <label>&nbsp;</label>
                        <button id="cAddCustomer" type="button" class="btn btn-success form-control">+</button>
                    </div>
                </div>
            </div>

            <div class="line"></div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Asegurado</th>
                        <th>Tipo Documento</th>
                        <th>Num Documento</th>
                        <th>Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="dataCustomer">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-lila">
        <div class="panel-body">
            <div class="line">Afiliados</div>

            <div class="container-fluid">

                <div class="form-group col-lg-2">
                    <label>Clase: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" name="class" value=""></g:textField>
                </div>

                <div class="form-group col-lg-2">
                    <label>No Documento: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" name="document" value=""></g:textField>
                </div>
                <div class="form-group col-lg-1">
                    <label>&nbsp;</label>
                    <button id="btnAfiliado" type="button" class="btn btn-success form-control">+</button>
                </div>
            </div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>id</th>
                        <th>Clase</th>
                        <th>Numero Documento</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="classTable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group" id="titularDiv">
    <div class="panel panel-border-orange">
        <div class="panel-body">
            <div class="line">Titular/Dependientes</div>

            <div class="container-fluid">
                <div class="form-group col-lg-3">
                    <label>No Documento: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" name="tidentification" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Nombres: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" name="tname" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Genero: </label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" id="tgender" name="tgender" from="${['M': 'Masculino','F':'Femenino']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Fecha Nac: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control datepicker" name="tbirthday" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>No Certificado: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" name="tcertified" value=""></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Plan: </label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" id="tplan" name="tplan" from="${business.Plan.list()}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Cobertura: </label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" id="tcoverage" name="tcoverage" from="${['LIFE': 'Vida','MASISTANCE':'Asistencia Medica', 'LMA':'Vida y Asistencia Medica']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>
                <div class="form-group col-lg-2">
                    <label>&nbsp;</label>
                    <button id="btnTitular" type="button" class="btn btn-primary form-control">Agregar</button>
                </div>
            </div>

            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Id</th>
                        <th>Numero Documento</th>
                        <th>Nombre</th>
                        <th>Genero</th>
                        <th>Fecha Nacimiento</th>
                        <th>No Certificado</th>
                        <th>Plan</th>
                        <th>Cobertura</th>
                        <th>Agregar/Eliminar</th>
                    </tr>
                    </thead>
                    <tbody id="titularTable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal Dependends-->
<div class="modal fade" id="dependentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width:750px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Dependientes</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group col-lg-6">
                        <label>No Documento: </label>
                        <span class="required-indicator">*</span>
                        <g:textField class="form-control" name="didentification" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Nombres: </label>
                        <span class="required-indicator">*</span>
                        <g:textField class="form-control" name="dname" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Genero: </label>
                        <span class="required-indicator">*</span>
                        <g:select class="form-control" id="coverage" name="dgender" from="${['M': 'Masculino','F':'Femenino']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Fecha Nac: </label>
                        <span class="required-indicator">*</span>
                        <g:textField class="form-control datepicker" name="dbirthday" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>No Certificado: </label>
                        <span class="required-indicator">*</span>
                        <g:textField class="form-control" name="dcertified" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Parentesco: </label>
                        <span class="required-indicator">*</span>
                        <g:select class="form-control" id="dcoverage" name="dcoverage" from="${['Spouse':'Conyugue', 'Child':'Hijo']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                    </div>
                    <div class="form-group col-lg-4">
                        <label>&nbsp;</label>
                        <button id="btnModalDep" type="button" class="btn btn-primary form-control">Agregar</button>
                    </div>
                </div>

                <div class="container-fluid">
                    <table class="table">
                        <thead class="thead-inverse">
                        <tr>
                            <th>id</th>
                            <th>Numero Documento</th>
                            <th>Nombre</th>
                            <th>Genero</th>
                            <th>Fecha Nacimiento</th>
                            <th>No Certificado</th>
                            <th>Parentesco</th>
                            <th>Eliminar</th>
                        </tr>
                        </thead>
                        <tbody id="modalTable">

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal New Customer -->
<div class="modal fade" id="addNewCustomer" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document" style="width:750px;">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="addModalLabel">Nuevo Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="form-group col-lg-6">
                        <label>No Documento: </label>
                        <span class="required-indicator">*</span>
                        <g:textField id="nidentification" class="form-control" name="nidentification" value=""></g:textField>
                    </div>
                    <div class="form-group col-lg-6">
                        <label>Nombre: </label>
                        <span class="required-indicator">*</span>
                        <g:textField id="nname" class="form-control" name="nname" value=""></g:textField>
                    </div>

                    <div class="form-group col-lg-3">
                        <label>Provincia</label>
                        <span class="required-indicator">*</span>
                        <g:select id="nprovince" class="form-control" name="nprovince" from="${geography.Province.list()}" noSelection="${['': '-- Seleccione --']}" optionKey="id" optionValue="name" value="${client?.province?.id}"></g:select>
                    </div>

                    <div class="form-group col-lg-3">
                        <label>Ciudad</label>
                        <span class="required-indicator">*</span>
                        <g:select id="ncity" class="form-control" name="ncity" from="${geography.City.list()}" noSelection="${['': '-- Seleccione --']}" value="${client?.city?.id}" optionKey="id" optionValue="name"></g:select>
                    </div>

                    <div class="form-group col-lg-3">
                        <label>Tipo Persona</label>
                        <g:select id="ntype" class="form-control" name="ntype" from="${['NATURAL', 'JURIDICA']}" noSelection="${['': '-- Seleccione --']}" value="${client?.type}"></g:select>
                    </div>

                    <div class="form-group col-lg-3">
                        <label>Fecha Nacimiento</label>
                        <g:textField id="nbirthdatetemp" class="form-control datepicker" name="nbirthdatetemp" value="${client?.birthdate?.toString()?.replace('-', '/')?.substring(0, 16)}"></g:textField>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Teléfono</label>
                        <span class="required-indicator">*</span>
                        <g:textField id="nphones" class="form-control" name="nphones" value="${client?.phones}"></g:textField>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Email</label>
                        <g:textField id="nemail" class="form-control" name="email" value="${client?.email}"></g:textField>
                    </div>

                    <div class="form-group col-lg-12">
                        <label>Dirección</label>
                        <span class="required-indicator">*</span>
                        <g:textArea id="naddress" class="form-control" name="naddress" value="${client?.address}"></g:textArea>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Persona de Contacto</label>
                        <g:textField id="ncontactPersonName" class="form-control" name="ncontactPersonName" value="${client?.contactPersonName}"></g:textField>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Teléfono de Contacto</label>
                        <g:textField id="ncontactPersonPhone" class="form-control" name="ncontactPersonPhone" value="${client?.contactPersonPhone}"></g:textField>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Email de Contacto</label>
                        <g:textField id="ncontactPersonEmail" class="form-control" name="ncontactPersonEmail" value="${client?.contactPersonEmail}"></g:textField>
                    </div>

                    <div class="form-group col-lg-6">
                        <label>Estado</label>
                        <g:select id="nstatus" class="form-control" name="nstatus" from="${['ACTIVO', 'INACTIVO']}" value="${client?.status}"></g:select>
                    </div>

                    <div class="form-group col-lg-12">
                        <label>Observaciones</label>
                        <g:textArea id="nobservations" class="form-control" name="nobservations" value="${client?.observations}"></g:textArea>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button id="saveNewClient" type="button" class="btn btn-success">Guardar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>