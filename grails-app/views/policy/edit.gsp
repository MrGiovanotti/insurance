<meta name="layout" content="main">

<asset:javascript src="policy/edit.js" />

<div>
    <g:form url="[resource:policy, action:'update']" method="PUT">
        <g:render template="formEdit"></g:render>
        <g:submitButton class="btn-save" name="btnSubmit" value="Actualizar"></g:submitButton>
    </g:form>
</div>