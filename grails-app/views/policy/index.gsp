<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="policy" action="create">Nueva Poliza</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Numero Poliza</th>
                <th>Nombre Cliente</th>
                <th>Caducidad Poliza</th>
                <th>% Comision Vida</th>
                <th>% Comision Salud</th>
                <th>% Comision Dental</th>

            </tr>
            </thead>
            <tbody>
                <g:each in="${policy}" var="policyInstance">
                    <tr>
                        <td><g:link controller="policy" action="show" id="${policyInstance.id}">${policyInstance.number}</g:link></td>
                        <td>${policyInstance.client.name}</td>
                        <td>${policyInstance.endDate}</td>
                        <td>${policyInstance.comLife}</td>
                        <td>${policyInstance.comHealth}</td>
                        <td>${policyInstance.comDental}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
    </div>
</div>