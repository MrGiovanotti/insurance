<meta name="layout" content="main">

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Informacion Poliza</div>
                <div class="container-fluid">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th scope="row">Numero Poliza</th>
                            <td>${policy.number}</td>
                        </tr>
                        <tr>
                            <th scope="row">Tipo</th>
                            <td>${policy.policyType == 'IN' ? 'Individual' : 'Colectivo'}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fecha Inicio</th>
                            <td>${policy.startDate}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fecha Fin</th>
                            <td>${policy.endDate}</td>
                        </tr>
                        <tr>
                            <th scope="row">Aseguradora</th>
                            <td>${policy.insurance.name}</td>
                        </tr>
                        <tr>
                            <th scope="row">Ramo</th>
                            <td>${policy.ramo.name}</td>
                        </tr>
                        <tr>
                            <th scope="row">Producto</th>
                            <td>${policy.product.name}</td>
                        </tr>
                     </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-red">
        <div class="panel-body">
            <div class="line">Informacion Cliente Titular</div>
            <div class="container-fluid">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th scope="row">Identificacion</th>
                        <td>${policy.client.identification}</td>
                    </tr>
                    <tr>
                        <th scope="row">Nombre</th>
                        <td>${policy.client.name}</td>
                    </tr>
                    <tr>
                        <th scope="row">Provincia</th>
                        <td>${policy.client.province.name}</td>
                    </tr>
                    <tr>
                        <th scope="row">Ciudad</th>
                        <td>${policy.client.city.name}</td>
                    </tr>
                    <tr>
                        <th scope="row">Telefono</th>
                        <td>${policy.client.phones}</td>
                    </tr>
                    <tr>
                        <th scope="row">Direccion</th>
                        <td>${policy.client.address}</td>
                    </tr>
                    <tr>
                        <th scope="row">Correo</th>
                        <td>${policy.client.email}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="line">Porcentaje de comisión</div>
            <div class="container-fluid">
                <table class="table table-striped">
                    <tbody>
                    <tr>
                        <th scope="row">Comisión Vida</th>
                        <td>${policy.comLife}%
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Comisión Salud</th>
                        <td>${policy.comHealth}%
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <th scope="row">Comisión Dental</th>
                        <td>${policy.comDental}%
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<g:link class="btn btn-primary" controller="policy" action="edit" id="${policy.id}">Editar</g:link>