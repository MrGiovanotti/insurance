<div class="panel-group">
    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="container-fluid">
                <div class="line">Informacion Poliza</div>
                <div class="form-group col-lg-3">
                    <label>Numero Poliza: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="number" value="${policy?.number}"></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Fecha Inicio</label>
                    <g:textField required="" class="form-control datepicker" name="startDateTemp" value="${policy?.startDate?.toString()?.replace('-', '/')?.substring(0, 16)}"></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Fecha Fin</label>
                    <g:textField required="" class="form-control datepicker" name="endDateTemp" value="${policy?.startDate?.toString()?.replace('-', '/')?.substring(0, 16)}"></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>Tipo Afiliacion</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="membershipType" from="${['IN': 'Individual','CO':'Colectivo']}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="key" optionValue="value"></g:select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Aseguradora</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="insurance" from="${business.Insurance.list()}" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Ramo</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="ramo" from="" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>
                <div class="form-group col-lg-3">
                    <label>Producto</label>
                    <span class="required-indicator">*</span>
                    <g:select class="form-control" name="product" from="" noSelection="${['': '-- Seleccione --']}" value="" optionKey="id" optionValue="name"></g:select>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-red1">
        <div class="panel-body">
            <div class="line">Comisiones</div>
            <div class="container-fluid">
                <div class="form-group col-lg-3">
                    <label>% Comision Vida: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="life" value="${policy?.comLife}"></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>% Comision Salud: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="health" value="${policy?.comHealth}"></g:textField>
                </div>
                <div class="form-group col-lg-3">
                    <label>% Comision Dental: </label>
                    <span class="required-indicator">*</span>
                    <g:textField class="form-control" required="" name="dental" value="${policy?.comDental}"></g:textField>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-black">
        <div class="panel-body">
            <div class="line">Clientes/Clases</div>
            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                        <th>Asegurado</th>
                        <th>Tipo Documento</th>
                        <th>Num Documento</th>
                    </tr>
                    </thead>
                    <tbody id="dataCustomer">
                        <td>${policy.client.name}</td>
                        <td>${policy.client.identification.toString().size() == '10' ? 'Cedula' : 'RUC' } </td>
                        <td>${policy.client.identification}</td>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="panel-group">
    <div class="panel panel-border-lila">
        <div class="panel-body">
            <div class="line">Afiliados</div>
            <div class="container-fluid">
                <table class="table">
                    <thead class="thead-inverse">
                        <tr>
                            <th>id</th>
                            <th>Clase</th>
                            <th>Numero Documento</th>
                            <th>Plan</th>
                            <th>Cobertura</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody id="classTable">

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>