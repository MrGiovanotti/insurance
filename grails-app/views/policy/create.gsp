<meta name="layout" content="main">
<asset:javascript src="usogeneral/moment.min.js" />
<asset:javascript src="policy/create.js" />
<asset:javascript src="usogeneral/bootstrap-datepicker.min.js" />
<asset:javascript src="usogeneral/bootstrap-datepicker.es.min.js" />
<asset:stylesheet src="usogeneral/bootstrap-datepicker.min.css" />
<asset:javascript src="usogeneral/customdatepicker.js" />

<div>
    <g:form action="save">
        <g:render template="form"></g:render>
        <g:submitButton class="btn btn-primary" name="btnSubmit" value="Guardar"></g:submitButton>
    </g:form>
</div>