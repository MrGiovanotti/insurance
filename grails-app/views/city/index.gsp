<meta name="layout" content="main">
<div><g:link class="btn btn-default" controller="city" action="create">Nueva Ciudad</g:link> </div>
<br>
<div>
    <div class="table-responsive">
        <table class="data-table">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Provincia</th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${cities}" var="cityInstance">
                <tr>
                    <td><g:link controller="city" action="edit" id="${cityInstance.id}">${cityInstance.name}</g:link></td>
                    <td>${cityInstance.province.name}</td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
</div>