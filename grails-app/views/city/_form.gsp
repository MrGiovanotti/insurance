<div class="container-fluid">
    <div class="form-group col-lg-3">
        <label>Nombre</label>
        <g:textField class="form-control" name="name" value="${city?.name}"></g:textField>
    </div>
    <div class="form-group col-lg-3">
        <label>Provincia:</label>
        <g:select class="form-control" required="" name="province" optionKey="id" from="${geography.Province.list()}" noSelection="${['': '-- Seleccione --']}" optionValue="name" value="${city?.id}"></g:select>
    </div>
</div>