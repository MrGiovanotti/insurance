<%@ page import="globalmethods.Util"%>

<div id="sidebar-links">
	<div class="menu-header">
		<i class="fa fa-fw fa-bars fa-lg"></i>
	</div>
	<ul class="nav">
		<li
			<g:if test="${actionName == "dashboard"}">
				class="nav-active"
			</g:if>>
			<a href="${createLink(uri: '/user/dashboard')}">
				<i class="fa fa-fw fa-dashboard"></i><span class="menu-name">&nbsp;Dashboard</span>
			</a>
		</li>
		<li
		<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "policy/index")}">
			<li class="nav-security
							<g:if test="${controllerName == "policy"}">
				nav-active subnav-expanded
			</g:if>
			">
				<a href="${createLink(uri: '/policy/index')}">
					<i class="fa fa-fw fa-qq"></i><span class="menu-name">&nbsp;Polizas</span>
				</a>
			</li>
		</g:if>
		</li>
		<li
		<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "sinister/index")}">
			<li class="nav-security
							<g:if test="${controllerName == "sinister"}">
				nav-active subnav-expanded
			</g:if>
			">
				<a href="${createLink(uri: '/sinister/index')}">
					<i class="fa fa-fw fa-magnet"></i><span class="menu-name">&nbsp;Siniestros</span>
				</a>
			</li>
		</g:if>
		</li>

		<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "permission/index") || globalmethods.Util.checkAccess(session.user?.username?.toString(), "rol/index") || globalmethods.Util.checkAccess(session.user?.username?.toString(), "user/index")}">
			<li>
				<a class="parent-sub" data-toggle="collapse" data-target="#sub-security">
					<i class="fa fa-fw fa-lock sidebar-icon"></i><span class="menu-name">&nbsp;Seguridad</span>
					<i class="fa fa-fw fa-caret-down"></i>
				</a>
				<ul id="sub-security" class="collapse nav-sub">
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "permission/index")}">
						<li class="nav-security
							<g:if test="${controllerName == "permission"}">
                            	nav-active subnav-expanded
                        	</g:if>
                        ">
							<a href="${createLink(uri: '/permission/index')}">
								<i class="fa fa-fw fa-star"></i><span class="menu-name">&nbsp;Permisos</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "rol/index")}">
						<li class="nav-security
							<g:if test="${controllerName == "rol"}">
                            	nav-active subnav-expanded
	                        </g:if>
                        ">
							<a href="${createLink(uri: '/rol/index')}">
								<i class="fa fa-fw fa-th"></i><span class="menu-name">&nbsp;Roles</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "user/index")}">
						<li class="nav-security
							<g:if test="${controllerName == "user" && actionName == "index"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/user/index')}">
								<i class="fa fa-fw fa-th"></i><span class="menu-name">&nbsp;Usuarios</span>
							</a>
						</li>
					</g:if>
				</ul>
			</li>
		</g:if>

		<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "province/index") || globalmethods.Util.checkAccess(session.user?.username?.toString(), "city/index")}">
			<li>
				<a class="parent-sub" data-toggle="collapse" data-target="#sub-catalogos">
					<i class="fa fa-fw fa-database sidebar-icon"></i><span class="menu-name">&nbsp;Catálogos</span>
					<i class="fa fa-fw fa-caret-down"></i>
				</a>
				<ul id="sub-catalogos" class="collapse nav-sub">
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "province/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "province"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/province/index')}">
								<i class="fa fa-fw fa-flag-checkered"></i><span class="menu-name">&nbsp;Provincias</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "city/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "city"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/city/index')}">
								<i class="fa fa-fw fa-flag-o"></i><span class="menu-name">&nbsp;Ciudades</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "client/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "client"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/client/index')}">
								<i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Clientes</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "insurance/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "insurance"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/insurance/index')}">
								<i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Aseguradoras</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "plan/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "plan"}">
							nav-active subnav-expanded
							</g:if>
						">
							<a href="${createLink(uri: '/plan/index')}">
								<i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Planes</span>
							</a>
						</li>
					</g:if>
                    <g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "ramo/index")}">
                        <li class="nav-catalogos
							<g:if test="${controllerName == "ramo"}">
                            nav-active subnav-expanded
                        </g:if>
                        ">
                            <a href="${createLink(uri: '/ramo/index')}">
                                <i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Ramos</span>
                            </a>
                        </li>
                    </g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "product/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "product"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/product/index')}">
								<i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Productos</span>
							</a>
						</li>
					</g:if>
					<g:if test="${globalmethods.Util.checkAccess(session.user?.username?.toString(), "diagnosis/index")}">
						<li class="nav-catalogos
							<g:if test="${controllerName == "diagnosis"}">
							nav-active subnav-expanded
						</g:if>
						">
							<a href="${createLink(uri: '/diagnosis/index')}">
								<i class="fa fa-fw fa-users"></i><span class="menu-name">&nbsp;Diagnosticos</span>
							</a>
						</li>
					</g:if>
				</ul>
			</li>
		</g:if>
		<li class="box-workaround-bug">&nbsp;</li>

	</ul>
</div>
