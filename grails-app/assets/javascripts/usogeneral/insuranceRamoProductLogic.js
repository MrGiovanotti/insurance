$(document).ready(function () {
    $("#insurance").change(function () {
        emptySelect("ramo");
        if($("#product").val())
            emptySelect("product");
        if($("#insurance").val() != ""){
            $idInsurance = this.value;
            $.get(baseUrl + "/BusinessAjax/getRamosByInsurance", {id: $idInsurance}, function (data) {
                fillSelect("ramo", data);
            });
        }
    });

    $("#ramo").change(function () {
       emptySelect("product");
       if($("#ramo").val() != ""){
           $ramo = this.value;
           $.get(baseUrl + "/BusinessAjax/getProductsByRamo", {id: $ramo}, function (data) {
               fillSelect("product", data);
           });
       }
    });

});

//This function empties a select component
function emptySelect(idSelect) {

    var select = document.getElementById(idSelect);
    var option = document.createElement('option');
    var NumberItems = select.length;

    while (NumberItems > 0) {
        NumberItems--;
        select.remove(NumberItems);
    }

    option.text = '-- Seleccione --';
    option.value = ''
    select.add(option, null);
}

//This function fills a select component
function fillSelect(idSelect, data) {

    var select = document.getElementById(idSelect);
    var numberSubstatus = data[0].length;

    emptySelect(idSelect)

    if (numberSubstatus > 0) {
        for (i = 0; i < numberSubstatus; i++) {
            var option = document.createElement('option');
            option.text = data[1][i];
            option.value = data[0][i];
            select.add(option, null);
        }
    }

    return numberSubstatus;
}