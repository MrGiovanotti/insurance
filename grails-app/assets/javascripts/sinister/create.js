$(function () {

    $spoliza = $("#spoliza");
    $spatient = $("#spatient");
    $diagnosis = $("#diagnosis");
    $stype = $("#stype");
    $invoiceValue = $("#invoiceValue");
    $observations = $("#observations");
    $btnSinister = $("#btnSinister");
    $btnSearchSinister = $("#btnSearchSinister");
    $btnAddHolder = $("#btnAddHolder");
    $count = 1;
    $calc = 0;

    $("#totalInvoice").val("Total Factura: " + $calc);
    localStorage.setItem("calc", $calc);

    $btnSearchSinister.click(function () {
        policySearch($spoliza.val());
        searchHolders($spoliza.val());
        $spoliza.val("");
    });

    $btnSinister.click(function () {

        $("#sinisterTable").append('<tr class="sinister_' + $count  + '">' +
        '<td>' + "54555"  + '</td>' +
        '<td>' + $("#spatient option:selected").text() + '</td>' +
        '<td>' + $("#diagnosis option:selected").text() + '</td>' +
        '<td>' + $("#stype option:selected").text() + '</td>' +
        '<td>' + $invoiceValue.val() + '</td>' +
        '<td>' + $observations.val() + '</td>' +
        '<td>' + '<button id="deleteSinister" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + $invoiceValue.val() + "," + '3' +')">X</button>'+'</td>' +
        '</tr>');

        $count++;

        $calc =  parseFloat(localStorage.getItem('calc')) + parseFloat($invoiceValue.val());
        localStorage.setItem('calc',$calc);

        $("#totalInvoice").val("Total Factura: " + parseFloat($calc).toFixed(2));

        $spatient.val("");
        $stype.val("");
        $invoiceValue.val("");
        $diagnosis.val("");
        $observations.val("");
    });

    $btnAddHolder.click(function () {
        addHolderInformation($("#holders option:selected").val());
    });
});

function deleteItem(id, value, type) {

    if(type == '3'){

        $(".sinister_" + id).remove();

        $newTotal = parseFloat(localStorage.getItem("calc")) - parseFloat(value);
        $("#totalInvoice").val("Total Factura: " + parseFloat($newTotal).toFixed(2));
        localStorage.setItem('calc',$newTotal);

    }else if(type == '1'){
        $(".policy_" + id).remove();
        $("#holders").children('option:not(:first)').remove();

    }else if(type == '2'){
        $(".holder_" + id).remove();
    }

}

function policySearch(policy) {

    $.post(baseUrl + "/BusinessAjax/searchPolicy/",{
        policy : policy
    }).done(function(data) {
        if($('#dataPolicy tr').length <= 1 && data != '') {
            $("#dataPolicy").append('<tr class="policy_' + $count + '">' +
                '<td>' + data.number + '</td>' +
                '<td>' + data.name  + '</td>' +
                '<td>' + data.ramo + '</td>' +
                '<td>' + data.product + '</td>' +
                '<td>' + '<button id="deleteSinister" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '0' + "," + '1' +')">X</button>'+'</td>' +
                '</tr>');
        }
    });
}

function searchHolders(policyNumber){
    $.post(baseUrl + "/BusinessAjax/searchHolders/",{

        policyNumber : policyNumber

    }).done(function(data) {

        $.each(data, function(item, element) {
            $("#holders").append($("<option />").val(element.id).text(element.name));
        });
    });
}

function addHolderInformation(id){
    $.post(baseUrl + "/BusinessAjax/getInformationHolder/",{

        holderId : id

    }).done(function(data) {
        if($('#dataCustomer tr').length == 0 && data != '') {
            $("#dataCustomer").append('<tr class="holder_' + $count + '">' +
                '<td>' + data.identfication + '</td>' +
                '<td>' + data.name  + '</td>' +
                '<td>' + data.birthdate  + '</td>' +
                '<td>' + data.certificateNumber  + '</td>' +
                '<td>' + '<button id="deleteSinister" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '0' + "," + '2' +')">X</button>'+'</td>' +
                '</tr>');
        }
    });
}