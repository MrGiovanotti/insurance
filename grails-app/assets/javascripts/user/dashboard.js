$(function () {

    //Del portlet.js
    $(document).on('click', '.panel-heading .btn-clickable', function(e){
        var $this = $(this);
        if(!$this.hasClass('panel-collapsed')) {
            $this.parents('.panel').find('.panel-body').slideUp();
            $this.addClass('panel-collapsed');
            $this.find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down');
        } else {
            $this.parents('.panel').find('.panel-body').slideDown();
            $this.removeClass('panel-collapsed');
            $this.find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');
        }
    });
    //-----------------------------------

    $cuadro = $(".cuadro");
    $cuadro.on("mouseenter", function(){
        $(".fa-5x", this).addClass("icon-anim");
    });
    $cuadro.on("mouseleave", function(){
        $(".fa-5x", this).removeClass("icon-anim");
    });
    $cuadro.on("mouseenter", function(){
        $(".huge", this).addClass("icon-anim");
    });
    $cuadro.on("mouseleave", function(){
        $(".huge", this).removeClass("icon-anim");
    });



            $('#container').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: 'Exitosos y Contactos por Hora'
                },
                subtitle: {
                    text: 'Encuestas'
                },
                xAxis: {
                    categories: ['9', '10', '11', '12', '13'],
                    crosshair: true,
                    title: {
                        text: 'Horas'
                    }
                },
                yAxis: {
                    min: 0,
                    title: {
                        text: 'Cantidad'
                    }
                },
                tooltip: {
                    headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                    pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.0f} </b></td></tr>',
                    footerFormat: '</table>',
                    shared: true,
                    useHTML: true
                },
                plotOptions: {
                    column: {
                        pointPadding: 0.2,
                        borderWidth: 0
                    }
                },
                series: [{
                    name: 'Contactados',
                    data: [5, 3, 5, 6, 1]

                }, {
                    name: 'Exitosos',
                    data: [4, 0, 3, 6, 0]

                }]
            });







//	Grafico pie con leyenda CU1


            $('#containerpieCu1').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'GRAFICO 1'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Cantidad',
                    colorByPoint: true,
                    data: [{
                        name: 'Microsoft Internet Explorer',
                        y: 56.33
                    }, {
                        name: 'Chrome',
                        y: 24.03,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Firefox',
                        y: 10.38
                    }, {
                        name: 'Safari',
                        y: 4.77
                    }, {
                        name: 'Opera',
                        y: 0.91
                    }, {
                        name: 'Proprietary or Undetectable',
                        y: 0.2
                    }]
                }]
            });





            $('#containerpieCu2').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'GRAFICO 2'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Cantidad',
                    colorByPoint: true,
                    data: [{
                        name: 'Microsoft Internet Explorer',
                        y: 56.33
                    }, {
                        name: 'Chrome',
                        y: 24.03,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Firefox',
                        y: 10.38
                    }, {
                        name: 'Safari',
                        y: 4.77
                    }, {
                        name: 'Opera',
                        y: 0.91
                    }, {
                        name: 'Proprietary or Undetectable',
                        y: 0.2
                    }]
                }]
            });





            $('#containerpieCu3').highcharts({
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: null,
                    plotShadow: false,
                    type: 'pie'
                },
                title: {
                    text: 'GRAFICO 3'
                },
                tooltip: {
                    pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: true,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: false
                        },
                        showInLegend: true
                    }
                },
                series: [{
                    name: 'Cantidad',
                    colorByPoint: true,
                    data: [{
                        name: 'Microsoft Internet Explorer',
                        y: 56.33
                    }, {
                        name: 'Chrome',
                        y: 24.03,
                        sliced: true,
                        selected: true
                    }, {
                        name: 'Firefox',
                        y: 10.38
                    }, {
                        name: 'Safari',
                        y: 4.77
                    }, {
                        name: 'Opera',
                        y: 0.91
                    }, {
                        name: 'Proprietary or Undetectable',
                        y: 0.2
                    }]
                }]
            });








            $('#containerpiedd').highcharts({
                chart: {
                    type: 'pie'
                },
                title: {
                    text: 'Browser market shares. January, 2015 to May, 2015'
                },
                subtitle: {
                    text: 'Click the slices to view versions. Source: netmarketshare.com.'
                },
                plotOptions: {
                    series: {
                        dataLabels: {
                            enabled: true,
                            format: '{point.name}: {point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    data: [{
                        name: 'IE',
                        y: 56.33,
                        drilldown: 'IE'
                    }, {
                        name: 'Chrome',
                        y: 24.03,
                        drilldown: 'Chrome'
                    }, {
                        name: 'Firefox',
                        y: 10.38,
                        drilldown: 'Firefox'
                    }, {
                        name: 'Safari',
                        y: 4.77,
                        drilldown: 'Safari'
                    }, {
                        name: 'Opera',
                        y: 0.91,
                        drilldown: 'Opera'
                    }, {
                        name: 'Proprietary or Undetectable',
                        y: 0.2,
                        drilldown: null
                    }]
                }],
                drilldown: {
                    series: [{
                        name: 'IE',
                        id: 'IE',
                        data: [
                            ['v11.0', 24.13],
                            ['v8.0', 17.2],
                            ['v9.0', 8.11],
                            ['v10.0', 5.33],
                            ['v6.0', 1.06],
                            ['v7.0', 0.5]
                        ]
                    }, {
                        name: 'Chrome',
                        id: 'Chrome',
                        data: [
                            ['v40.0', 5],
                            ['v41.0', 4.32],
                            ['v42.0', 3.68],
                            ['v39.0', 2.96],
                            ['v36.0', 2.53],
                            ['v43.0', 1.45],
                            ['v31.0', 1.24],
                            ['v35.0', 0.85],
                            ['v38.0', 0.6],
                            ['v32.0', 0.55],
                            ['v37.0', 0.38],
                            ['v33.0', 0.19],
                            ['v34.0', 0.14],
                            ['v30.0', 0.14]
                        ]
                    }, {
                        name: 'Firefox',
                        id: 'Firefox',
                        data: [
                            ['v35', 2.76],
                            ['v36', 2.32],
                            ['v37', 2.31],
                            ['v34', 1.27],
                            ['v38', 1.02],
                            ['v31', 0.33],
                            ['v33', 0.22],
                            ['v32', 0.15]
                        ]
                    }, {
                        name: 'Safari',
                        id: 'Safari',
                        data: [
                            ['v8.0', 2.56],
                            ['v7.1', 0.77],
                            ['v5.1', 0.42],
                            ['v5.0', 0.3],
                            ['v6.1', 0.29],
                            ['v7.0', 0.26],
                            ['v6.2', 0.17]
                        ]
                    }, {
                        name: 'Opera',
                        id: 'Opera',
                        data: [
                            ['v12.x', 0.34],
                            ['v28', 0.24],
                            ['v27', 0.17],
                            ['v29', 0.16]
                        ]
                    }]
                }
            });



            $('#containerlinereport').highcharts({
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Siniestros en los últimos 7 días'
                },
                subtitle: {
                    text: 'Siniestros Prevenir'
                },
                xAxis: {
                    categories: ['2018-01-01', '2018-01-02', '2018-01-03', '2018-01-04', '2018-01-05', '2018-01-06', '2018-01-07']
                },
                yAxis: {
                    title: {
                        text: 'Cantidad Siniestros'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Cantidad Adicionales',
                    data: [15, 10, 23, 7, 5, 20, 3]
                }]
            });


});


