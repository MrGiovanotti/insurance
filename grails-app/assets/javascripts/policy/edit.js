$(function () {

    getClass($("#number").val());

    //allow edit table
    $("#dataCustomer td").click(function () {

        var col = $(this).parent().children().index($(this));

        if(col == '0' || col == '2'){

            var OriginalContent = $(this).text();

            if($('#dataCustomer td').eq(0) || $('#dataCustomer td').eq(2)){

                $(this).addClass("cellEditing");
                $(this).html('<input name="customer" type="text" value="' + OriginalContent + '">');
                $(this).children().first().focus();

                $(this).children().first().keypress(function (e) {
                    if (e.which == 13) {
                        var newContent = $(this).val();
                        $(this).parent().text(newContent);
                        $(this).parent().removeClass("cellEditing");
                    }
                });

                $(this).children().first().blur(function() {
                    var newContent = $(this).val();
                    $(this).parent().text(newContent);
                    $(this).parent().removeClass("cellEditing");
                });
            }
        }
    });
});

function getClass(idNumber) {

    $.post(baseUrl + "/BusinessAjax/getClassContainer/",{

        idNumber : idNumber

    }).done(function(data) {
        $.each(data, function (index, element) {
            $("#classTable").append('<tr class="classes_' + ''  + '">' +
                '<input name="afi[' + 'classId' + ']" type="hidden" value="' + '' +'">'  +
                '<input name="afi[' + 'identification' + ']" type="hidden" value="' + '' +'">'  +
                '<input name="afi[' + 'plan' + ']" type="hidden" value="' + '' +'">'  +
                '<input name="afi[' + 'coverage' + ']" type="hidden" value="' + '' +'">'  +
                '<td>' + data[index].id  + '</td>' +
                '<td>' + data[index].name + '</td>' +
                '<td>' + data[index].documentNumber + '</td>' +
                '<td>' + data[index].plan.name + '</td>' +
                '<td>' + data[index].coverage + '</td>' +
                '<td>' + '<button id="titulares_' + '' +'" type="button" class="btn btn-primary" onclick="showTitulares(' + '' + "," + '' + "," + "'" + ''   + "'" + ')"> Ver/Agregar Titulares </button>' + " "
                + '<button id="deleteClass" type="button" class="btn btn-danger" onclick="deleteItem(' + '' + "," + '4' + ')">X</button>'+'</td>' +
                '</tr>');
        });
    });
}