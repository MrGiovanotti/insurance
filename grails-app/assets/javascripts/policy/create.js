$(function () {

    //Afiliados
    $class = $("#class");
    $document = $("#document");
    $coverage = $("#coverage");
    $btnAfiliado = $("#btnAfiliado");

    //Titular
    $tidentification = $("#tidentification");
    $tname = $("#tname");
    $tgender = $("#tgender");
    $tbirthday = $("#tbirthday");
    $tcertified = $("#tcertified");
    $btnTitular = $("#btnTitular");

    //Modal form
    $didentification = $("#didentification");
    $dname = $("#dname");
    $dbirthday = $("#dbirthday");
    $dcertified = $("#dcertified");
    $btnModalDep = $("#btnModalDep");

    //Modal Clients
    $saveNewClient = $("#saveNewClient");
    $nidentification = $("#nidentification");
    $nname = $("#nname");
    $nprovince = $("#nprovince");
    $ncity = $("#ncity");
    $ntype = $("#ntype");
    $nbirthdatetemp = $("#nbirthdatetemp");
    $nphones = $("#nphones");
    $nemail = $("#nemail");
    $naddress = $("#naddress");
    $ncontactPersonName = $("#ncontactPersonName");
    $ncontactPersonPhone = $("#ncontactPersonPhone");
    $ncontactPersonEmail = $("#ncontactPersonEmail");
    $nstatus = $("#nstatus");
    $nobservations = $("#nobservations");

    //client search
    $cidentification = $("#cIdentification");
    $btnByIdentification = $("#btnByIdentification");
    $addCustomer = $("#cAddCustomer");

    //initial Values
    $("#titularDiv").hide();
    $("#seachByName").hide();

    //Add button afiliado
    $count = 1;

    $btnAfiliado.click(function (e) {

        if($("#class").val().trim() == "" || $("#document").val().trim() == ""){
            ShowModal("","Uno de los campos se encuentran vacios");
            e.preventDefault();
            return false;
        }

        $duplicateClass = searchInTable($class.val());

        if($duplicateClass == false){

            $plan = $("#plan option:selected").val();

            $("#classTable").append('<tr class="classes_' + $count  + '">' +
                '<input name="afi[' + 'classId' + ']" type="hidden" value="' + $class.val() +'">'  +
                '<input name="afi[' + 'identification' + ']" type="hidden" value="' + $document.val() +'">'  +
                '<td>' + $count  + '</td>' +
                '<td>' + $class.val() + '</td>' +
                '<td>' + $document.val() + '</td>' +
                '<td>' + '<button id="titulares_' + $count +'" type="button" class="btn btn-primary" onclick="showTitulares(' + $count + "," + "'" + $class.val() + "'" + ')"> Ver/Agregar Titulares </button>' + " "
                       + '<button id="deleteClass" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '4' + ')">X</button>'+'</td>' +
                '</tr>');

            $count++;

            $class.val("");
            $document.val("");
            $coverage.val("");

        }else{
            ShowModal("","La clase ya se encuentra agregada");
        }
    });

    //Add button titular
    $btnTitular.click(function () {

        if($("#tidentification").val().trim() == ""){
            ShowModal("","El campo identificacion se encuentra vacio.");
            e.preventDefault();
            return false;
        }

        if($("#tname").val().trim() == ""){
            ShowModal("","El campo nombre se encuentra vacio.");
            e.preventDefault();
            return false;
        }

        if($("#tbirthday").val().trim() == ""){
            ShowModal("","El campo fecha de nacimiento se encuentra vacio.");
            e.preventDefault();
            return false;
        }

        if($("#tcertified").val().trim() == ""){
            ShowModal("","El campo no certificado se encuentra vacio.");
            e.preventDefault();
            return false;
        }

        localStorage.setItem('planType', $("#tplan option:selected").val());

        if(localStorage.getItem("planType") == '1'){
            $("#titularTable").append('<tr class="titulares_' + $count + " " + "classes_" + localStorage.getItem("class") + '">' +
                '<input name="classId" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tclass' + ']" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tidentification' + ']" type="hidden" value="' + $tidentification.val() +'">'  +
                '<input name="holder[' + 'tname' + ']" type="hidden" value="' + $tname.val() +'">'  +
                '<input name="holder[' + 'tgender' + ']" type="hidden" value="' + $("#tgender option:selected").val() +'">'  +
                '<input name="holder[' + 'tbirthday' + ']" type="hidden" value="' + $tbirthday.val() +'">'  +
                '<input name="holder[' + 'tcertified' + ']" type="hidden" value="' + $tcertified.val() +'">'  +
                '<input name="holder[' + 'tplan' + ']" type="hidden" value="' + $("#tplan option:selected").text() +'">'  +
                '<input name="holder[' + 'tcoverage' + ']" type="hidden" value="' + $("#tcoverage option:selected").text() +'">'+
                '<td>' + $count  + '</td>' +
                '<td>' + $tidentification.val() + '</td>' +
                '<td>' + $tname.val() + '</td>' +
                '<td>' + $("#tgender option:selected").text() + '</td>' +
                '<td>' + $tbirthday.val() + '</td>' +
                '<td>' + $tcertified.val() + '</td>' +
                '<td>' + $("#tplan option:selected").text() + '</td>' +
                '<td>' + $("#tcoverage option:selected").text() + '</td>' +
                '<td>' + '<button id="deleteTitular" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '1' + ')">X</button>'+'</td>' +
                '</tr>');
        }else if(localStorage.getItem("planType") == '2'){
            $("#titularTable").append('<tr class="titulares_' + $count + " " + "classes_" + localStorage.getItem("class") + '">' +
                '<input name="classId" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tclass' + ']" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tidentification' + ']" type="hidden" value="' + $tidentification.val() +'">'  +
                '<input name="holder[' + 'tname' + ']" type="hidden" value="' + $tname.val() +'">'  +
                '<input name="holder[' + 'tgender' + ']" type="hidden" value="' + $("#tgender option:selected").val() +'">'  +
                '<input name="holder[' + 'tbirthday' + ']" type="hidden" value="' + $tbirthday.val() +'">'  +
                '<input name="holder[' + 'tcertified' + ']" type="hidden" value="' + $tcertified.val() +'">'  +
                '<input name="holder[' + 'tplan' + ']" type="hidden" value="' + $("#tplan option:selected").text() +'">'  +
                '<input name="holder[' + 'tcoverage' + ']" type="hidden" value="' + $("#tcoverage option:selected").text() +'">'+
                '<td>' + $count  + '</td>' +
                '<td>' + $tidentification.val() + '</td>' +
                '<td>' + $tname.val() + '</td>' +
                '<td>' + $("#tgender option:selected").text() + '</td>' +
                '<td>' + $tbirthday.val() + '</td>' +
                '<td>' + $tcertified.val() + '</td>' +
                '<td>' + $("#tplan option:selected").text() + '</td>' +
                '<td>' + $("#tcoverage option:selected").text() + '</td>' +
                '<td>' + '<button id="addDepend_' + $count +'" type="button" class="btn btn-success" onclick="showDepends(' + "'" + $tidentification.val() + "'" + ')">\n' +
                '                    +\n' +
                '         </button>' + ' ' +'<button id="deleteTitular" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '1' + ')">X</button>'+'</td>' +
                '</tr>');
        }else if(localStorage.getItem("planType") == '3'){
            $("#titularTable").append('<tr class="titulares_' + $count + " " + "classes_" + localStorage.getItem("class") +'">' +
                '<input name="classId" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tclass' + ']" type="hidden" value="' + localStorage.getItem("class") +'">'  +
                '<input name="holder[' + 'tidentification' + ']" type="hidden" value="' + $tidentification.val() +'">'  +
                '<input name="holder[' + 'tname' + ']" type="hidden" value="' + $tname.val() +'">'  +
                '<input name="holder[' + 'tgender' + ']" type="hidden" value="' + $("#tgender option:selected").val() +'">'  +
                '<input name="holder[' + 'tbirthday' + ']" type="hidden" value="' + $tbirthday.val() +'">'  +
                '<input name="holder[' + 'tcertified' + ']" type="hidden" value="' + $tcertified.val() +'">'  +
                '<input name="holder[' + 'tplan' + ']" type="hidden" value="' + $("#tplan option:selected").text() +'">'  +
                '<input name="holder[' + 'tcoverage' + ']" type="hidden" value="' + $("#tcoverage option:selected").text() +'">'+
                '<td>' + $count  + '</td>' +
                '<td>' + $tidentification.val() + '</td>' +
                '<td>' + $tname.val() + '</td>' +
                '<td>' + $("#tgender option:selected").text() + '</td>' +
                '<td>' + $tbirthday.val() + '</td>' +
                '<td>' + $tcertified.val() + '</td>' +
                '<td>' + $("#tplan option:selected").text() + '</td>' +
                '<td>' + $("#tcoverage option:selected").text() + '</td>' +
                '<td>' + '<button id="addDepend_' + $count +'" type="button" class="btn btn-success" onclick="showDepends(' + "'" + $tidentification.val() + "'" + ')">\n' +
                '                    +\n' +
                '         </button>' + ' ' +'<button id="deleteTitular" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '1' + ')">X</button>'+'</td>' +
                '</tr>');

            $("#btnModalDep").show();
        }

        $count++;
    });

    $btnModalDep.click(function () {
        if(localStorage.getItem("planType") == '2'){
            $("#modalTable").append('<tr class="depend_' + $count + " " + "holderId_" + localStorage.getItem("holder") + '">' +
                '<input name="depend[' + 'dholder' + ']" type="hidden" value="' + localStorage.getItem("holder") +'">'  +
                '<input name="depend[' + 'didentification' + ']" type="hidden" value="' + $didentification.val() +'">'  +
                '<input name="depend[' + 'dname' + ']" type="hidden" value="' + $dname.val() +'">'  +
                '<input name="depend[' + 'dgender' + ']" type="hidden" value="' + $("#dgender option:selected").val() +'">'  +
                '<input name="depend[' + 'dbirthday' + ']" type="hidden" value="' + $dbirthday.val() +'">'  +
                '<input name="depend[' + 'dcertified' + ']" type="hidden" value="' + $dcertified.val() +'">'  +
                '<input name="depend[' + 'dcoverage' + ']" type="hidden" value="' + $("#dcoverage option:selected").text() +'">'  +
                '<td>' + $count + '</td>' +
                '<td>' + $didentification.val() + '</td>' +
                '<td>' + $dname.val() + '</td>' +
                '<td>' + $("#dgender option:selected").text() + '</td>' +
                '<td>' + $dbirthday.val() + '</td>' +
                '<td>' + $dcertified.val() + '</td>' +
                '<td>' + $("#dcoverage option:selected").text() + '</td>' +
                '<td>' + '<button id="deleteDepend" type="button" class="btn btn-danger form-control" onclick="deleteItem(' + $count + "," + '2' + ')">X</button>' + '</td>' +
                '</tr>');

            // $("#btnModalDep").hide();

        }else if(localStorage.getItem("planType") == '3'){
            $("#modalTable").append('<tr class="depend_' + $count + " " + "holderId_" + localStorage.getItem("holder") + '">' +
                '<input name="depend[' + 'dholder' + ']" type="hidden" value="' + localStorage.getItem("holder") +'">'  +
                '<input name="depend[' + 'didentification' + ']" type="hidden" value="' + $didentification.val() +'">'  +
                '<input name="depend[' + 'dname' + ']" type="hidden" value="' + $dname.val() +'">'  +
                '<input name="depend[' + 'dgender' + ']" type="hidden" value="' + $("#dgender option:selected").val() +'">'  +
                '<input name="depend[' + 'dbirthday' + ']" type="hidden" value="' + $dbirthday.val() +'">'  +
                '<input name="depend[' + 'dcertified' + ']" type="hidden" value="' + $dcertified.val() +'">'  +
                '<input name="depend[' + 'dcoverage' + ']" type="hidden" value="' + $("#dcoverage option:selected").text() +'">'  +
                '<td>' + $count + '</td>' +
                '<td>' + $didentification.val() + '</td>' +
                '<td>' + $dname.val() + '</td>' +
                '<td>' + $("#dgender option:selected").text() + '</td>' +
                '<td>' + $dbirthday.val() + '</td>' +
                '<td>' + $dcertified.val() + '</td>' +
                '<td>' + $("#dcoverage option:selected").text() + '</td>' +
                '<td>' + '<button id="deleteDepend" type="button" class="btn btn-danger form-control" onclick="deleteItem(' + $count + "," + '2' + ')">X</button>' + '</td>' +
                '</tr>');

            $count++;
        }

        $didentification.val("");
        $dname.val("");
        $dbirthday.val("");
        $dcertified.val("");
        $("#dgender").val("");
        $("#dcoverage").val("");
    });

    //search select 
    $("#searchby").change(function () {
        if($(this).val() == 'CI'){
            $("#seachByName").hide();
            $("#seachByIdentification").show();
        }else if($(this).val() == 'BYNAME'){
            $("#seachByName").show();
            $("#seachByIdentification").hide();
        }else if($(this).val() == 'NEW'){
            $("#addNewCustomer").modal();
        }
    })
    
    //search by identification button
    $btnByIdentification.click(function () {
        seachCustomer($cidentification.val());
    });

    $addCustomer.click(function () {
        if($('#dataCustomer tr').length == 0) {

            //Document of the class = identification of client
            $document.val($("#cSearchCustomer option:selected").val());

            $("#dataCustomer").append('<tr class="insured_' + $count + '">' +
                '<td>' + $("#cSearchCustomer option:selected").text() + '</td>' +
                '<td>' + ($("#cSearchCustomer option:selected").val().length == 10? "Cedula" : "Ruc")  + '</td>' +
                '<td>' + $("#cSearchCustomer option:selected").val() + '</td>' +
                '<td>' + '<button id="deleteInsured" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '3' + ')">X</button>' + '</td>' +
                '</tr>');
        }
    });

    $saveNewClient.click(function () {
        addNewCustomer($nidentification.val(), $nname.val(), $("#nprovince option:selected").val(), $("#ncity option:selected").val(),
        $("#ntype option:selected").val(), $nbirthdatetemp.val(), $nbirthdatetemp.val(), $nemail.val(), $naddress.val(),
        $ncontactPersonName.val(), $ncontactPersonPhone.val(), $ncontactPersonEmail.val(), $("#nstatus option:selected").val(), $nobservations.val());
    });



    //When Tipo Afiliación is focused, if Fecha Fin is empty, it is filled automatically
    $("#membershipType").focus(function () {
       if($("#startDateTemp").val().trim() != ""){
           if($("#endDateTemp").val().trim() == ""){
               $("#endDateTemp").val(moment($("#startDateTemp").val().trim()).add(1, 'years').format("YYYY/MM/DD"));
           }
       }
    });

});

function showTitulares(id, classType) {

    $tidentification.val("");
    $tname.val("");
    $tgender.val("");
    $tbirthday.val("");
    $tcertified.val("");

    localStorage.setItem("class", classType);

    $("#classTable").parents('table').find('tr').each( function( index, element ) {
        $(element).removeClass('highlight');
        $('#classTable tr').eq(id - 1).addClass('highlight');
    });

    //Hide if class not exist in table titularTable
    if($("#titularTable").find('th').not('.classes_' + localStorage.getItem("class"))){
        $("#titularTable tr").hide();
    }

    //Only show if class exist in table titularTable
    if($("#titularTable tr").is('.classes_' + localStorage.getItem("class"))){
        $('.classes_' + localStorage.getItem("class")).show();
    }

    $("#titularDiv").show();
}

function showDepends(holder) {

    // show button for add depends if plan type is + familiar
    if(localStorage.getItem("planType") == "3"){
        $btnModalDep.show();
    }

    $("#dependentModal").modal();
    localStorage.setItem("holder", holder);

    //Hide if class not exist in table modalTable
    if($("#modalTable").find('th').not('.holderId_' + localStorage.getItem("holder"))){
        $("#modalTable tr").hide();
    }

    //Only show if class exist in table modalTable
    if($("#modalTable tr").is('.holderId_' + localStorage.getItem("holder"))){
        $('.holderId_' + localStorage.getItem("holder")).show();
    }
}

function deleteItem(id,type) {
    if(type == '1'){
        $(".titulares_" + id).remove();
    }else if(type == '2'){
        $(".depend_" + id).remove();

        if(localStorage.getItem("planType") == "2"){
            $btnModalDep.show();
        }

    }else if(type == "3"){
        $(".insured_" + id).remove();
    }else if(type == "4"){
        $(".classes_" + id).remove();
    }
}

function seachCustomer(identification){

    $count = 1;

    $.post(baseUrl + "/BusinessAjax/searchCustomer/",{

        identification : identification

    }).done(function(data) {

        if(data != '' && $('#dataCustomer tr').length == 0) {
            $("#dataCustomer").append('<tr class="insured_' + $count + '">' +
                '<td>' + data.identification + '</td>' +
                '<td>' + (data.identification.length == 10 ? "Cedula" : "Ruc")  + '</td>' +
                '<td>' + data.name + '</td>' +
                '<td>' + '<button id="deleteInsured" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '3' + ')">X</button>' + '</td>' +
                '</tr>');
        }
    });
}

function addNewCustomer(identification, name, province, city, type, birthday, phone, mail, address, contactPName, contactPPhone, contactPMail, status, observations) {

    $.post(baseUrl + "/BusinessAjax/addNewCustomer/",{

        identification : identification,
        name : name,
        province : province,
        city : city,
        type : type,
        birthday : birthday,
        phone : phone,
        mail : mail,
        address : address,
        contactPName : contactPName,
        contactPPhone : contactPPhone,
        contactPMail : contactPMail,
        status : status,
        observations : observations

    }).done(function(data) {
       if(data == "true"){
           if($('#dataCustomer tr').length == 0) {
               $("#dataCustomer").append('<tr class="insured_' + $count + '">' +
                   '<td>' + identification + '</td>' +
                   '<td>' + (identification.length == 10? "Cedula" : "Ruc")  + '</td>' +
                   '<td>' + name + '</td>' +
                   '<td>' + '<button id="deleteInsured" type="button" class="btn btn-danger" onclick="deleteItem(' + $count + "," + '3' + ')">X</button>' + '</td>' +
                   '</tr>');
           }
           $('#addNewCustomer').modal('hide');
       }else{
           $('#addNewCustomer').modal('hide');
           ShowModal("", data);
       }
    });
}

function searchInTable(nameClass){

    var contents = {},
        duplicates = false;

    $("#classTable td").each(function() {

        $tdContent = $(this).text();

        if (contents[nameClass]) {
            duplicates = true;
            return false;
        }

        contents[$tdContent] = true;
    });

    if (duplicates) {
        return true;
    }else{
        return false;
    }
}
