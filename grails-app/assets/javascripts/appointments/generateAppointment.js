$(document).ready(function () {
    init();
    //When LOCATION is changed
    $("#location").change(function () {
        $("#visitAddressDiv").val("");
        if($("#location").val() == 'VISITA'){
            $("#visitAddressDiv").show();
        }else{
            $("#visitAddressDiv").hide();
        }
    });

    //When copy link is clicked
    $("#copy").click(function () {
        if($("#patient").val() == ''){
            alert("Debe seleccionar un paciente");
            return;
        }else{
            $id = $("#patient").val();
            $.get(baseUrl+"/AjaxPrescriptions/getPatientAddress", {id: $id}, function (data) {
                $("#visitAddress").val(data);
            })
        }
    })
})

function init() {
    $("#location").val($("#location option:first").val());
    $("#visitAddressDiv").hide();
}